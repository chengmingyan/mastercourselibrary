package cn.golfdigestchina.golfmaster.mastercourse.views;

import java.io.File;

import android.content.Context;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.CoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.common.Contexts;
import cn.golfdigestchina.golfmaster.mastercourse.utils.BitmapUtil;

public class ProgressDialogTitleView extends android.view.View {
	private View view;
	public ProgressDialogTitleView(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}
	public ProgressDialogTitleView(Context context,CoursesObject object){
		super(context);
		LayoutInflater inflater=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		view=inflater.inflate(R.layout.dialog_progress_titleview, null);
		ImageView logo = (ImageView) view.findViewById(R.id.image_logo);
		String logoName = object.getImages();
		if (logoName != null && !"".equals(logoName)) {
			File file = new File(Contexts.LOGO_PATH + logoName);
			if (file.exists()) {
				Bitmap logoBitmap = BitmapUtil.readBitMapByNative(Contexts.LOGO_PATH + logoName);
				logo.setImageBitmap(logoBitmap);
			}
		}else {
			logo.setImageResource(R.drawable.logo);
		}
		TextView tv_name = (TextView) view.findViewById(R.id.tv_title);
		tv_name.setText(object.getCoursesName());
	}
	public View getView() {
		return view;
	}
	public void setView(View view) {
		this.view = view;
	}
	
}
