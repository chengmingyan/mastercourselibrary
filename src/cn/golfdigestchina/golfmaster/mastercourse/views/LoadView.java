package cn.golfdigestchina.golfmaster.mastercourse.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;

public class LoadView extends LinearLayout {
	private OnStatusChangedListener onStatusChangedListener;
	private ImageButton image_tip;
	private ProgressBar bar;
	private TextView tv_tip;
	private Button btn_reload;
	private LinearLayout layout;
	private Status status = Status.loading;
	private OnClickListener onReLoadClickListener;

	public static enum Status {
		error, loading, successed, nodate
	}

	public LoadView(Context context, AttributeSet attrs) {
		super(context, attrs);
		View view = LayoutInflater.from(context).inflate(R.layout.loadview,
				null);
		this.bar = (ProgressBar) view.findViewById(R.id.progressBar1);
		this.image_tip = (ImageButton) view.findViewById(R.id.image_tip);
		btn_reload = (Button) view.findViewById(R.id.btn_reload);
		tv_tip = (TextView) view.findViewById(R.id.tv_tip);
		layout = (LinearLayout) view.findViewById(R.id.layout);
		this.addView(view);
	}

	public LoadView(Context context) {
		this(context, null);

	}


	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
		if (getOnStatusChangedListener() != null) {
			getOnStatusChangedListener().OnStatusChanged(status);
		}
		refresh();
	}
	
	private void refresh() {
		switch (status) {
		case error:
			layout.setVisibility(View.VISIBLE);
			image_tip.setImageResource(R.drawable.image_not_network);
			tv_tip.setText("网络错误,请检查网络!");
			bar.setVisibility(View.GONE);
			this.setVisibility(View.VISIBLE);
			break;
		case loading:
			layout.setVisibility(View.GONE);
			bar.setVisibility(View.VISIBLE);
			this.setVisibility(View.VISIBLE);
			break;
		case successed:
			layout.setVisibility(View.GONE);
			bar.setVisibility(View.GONE);
			this.setVisibility(View.GONE);
			break;
		case nodate:
			layout.setVisibility(View.VISIBLE);
			image_tip.setImageResource(R.drawable.image_not_data);
			tv_tip.setText("暂无内容,敬请期待!");
			bar.setVisibility(View.GONE);
			this.setVisibility(View.VISIBLE);
			break;
		default:
			break;
		}
	}

	public OnClickListener getOnReLoadClickListener() {
		return onReLoadClickListener;
	}

	public interface OnStatusChangedListener {
		public void OnStatusChanged(Status status);
	}

	public void setOnReLoadClickListener(OnClickListener onReLoadClickListener) {
		this.onReLoadClickListener = onReLoadClickListener;
		btn_reload.setOnClickListener(onReLoadClickListener);
	}

	public OnStatusChangedListener getOnStatusChangedListener() {
		return onStatusChangedListener;
	}

	public void setOnStatusChangedListener(
			OnStatusChangedListener onStatusChangedListener) {
		this.onStatusChangedListener = onStatusChangedListener;
	}

}
