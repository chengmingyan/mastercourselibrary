package cn.golfdigestchina.golfmaster.mastercourse.views;

import android.widget.ImageView;
import android.widget.TextView;

public class CourseListCellWrapper{
	public TextView courseName;//
	public ImageView courseRating;//
	public ImageView logo;//
	public TextView courseRange;//
	public ImageView ivCourseItem;
}
