package cn.golfdigestchina.golfmaster.mastercourse.views;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.os.Handler;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.activities.MapActivity;
import cn.golfdigestchina.golfmaster.mastercourse.beans.Point;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.models.MapModel;
import cn.golfdigestchina.golfmaster.mastercourse.utils.CoordinateUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.DistanceDataUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.DistanceUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.GpsUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.LocationUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.RightTriangleUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.ScreenUtil;

/**
 * 航拍图控件
 * 
 * @author chengmingyan
 */
public class AssistView extends View {

	private static final String TAG = AssistView.class.getName();
	private static float RADIUS;

	/**
	 * 左边图片的左起点坐标
	 */
	private final int LEFT_BITMAP_LEFT = 10;
	/**
	 * 左边果岭旗子的上起点坐标
	 */
	private final int LEFT_FlAG_TOP = 15;

	private Paint defaultPaint;
	private Paint greenFontPaint;
	private Paint arcLineFontPaint;
	private Paint marketPaint;
	private Paint cursorLinePaint;
	private Paint cursorFontPaint;

	/**
	 * 果岭旗子
	 */
	private Bitmap flagBmp;
	private Bitmap flagBmp1;
	private Bitmap flagBmp2;

	@SuppressWarnings("unused")
	private Point touchPoint;
	/**
	 * cursor在y轴上的偏移量
	 */
	public static float touch_offset_Y = 100;

	/**
	 * 按钮控件的偏移区域
	 */
	private static float image_offset = 40;
	private MapModel mapModel;
	private Paint paint;
	private static float buttonY = 60;
	/**
	 * 弧线值集合
	 */
	private int[] distanceArc;
	private static float cursor_textSize;// 弧线字体大小 480的20 720的22
	private static float arc_textSize;// 弧线字体大小 480的20 720的22
	private static float mark_textSize;// 弧线字体大小 480的20 720的22
	private static float green_textSize;// 果岭字体大小 480的20 720的22
	private static float image_bottom_offset = 30;// 480的30 720的60
	private CoordinateUtil util;
	private int index;
	private String distanceUnit_near;
	private float oneDistanceToPixel;
	private double longitude, latitude;
	private LocationUtil gpsUtil;
	private Handler handler;

	public double getLongitude() {
		longitude = gpsUtil.longitude;
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public double getLatitude() {
		latitude = gpsUtil.latitude;
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public AssistView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		init(context);
	}

	public AssistView(Context context, AttributeSet attrs) {
		super(context, attrs);
		init(context);
	}

	private void initBitmap() {

		// 指示果岭的图片
		flagBmp = BitmapFactory.decodeResource(getResources(),
				R.drawable.icon_distance);
		flagBmp1 = BitmapFactory.decodeResource(getResources(),
				R.drawable.image_green_distance1);
		flagBmp2 = BitmapFactory.decodeResource(getResources(),
				R.drawable.image_green_distance2);
	}

	private void initPaint() {
		paint = new Paint();
		paint.setStrokeWidth(2f);
		paint.setAntiAlias(true);
		paint.setStyle(Paint.Style.STROKE);

		touchPoint = new Point();
		cursorLinePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		cursorLinePaint.setStyle(Paint.Style.STROKE);
		cursorLinePaint.setColor(Color.WHITE);
		cursorLinePaint.setStrokeWidth(3f);

		marketPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		marketPaint.setStyle(Paint.Style.STROKE);
		marketPaint.setColor(Color.RED);
		marketPaint.setTextSize(mark_textSize);

		defaultPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		defaultPaint.setStyle(Paint.Style.STROKE);

		greenFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		greenFontPaint.setStyle(Paint.Style.STROKE);
		greenFontPaint.setColor(Color.WHITE);
		greenFontPaint.setTextSize(green_textSize);

		cursorFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		cursorFontPaint.setStyle(Paint.Style.STROKE);
		cursorFontPaint.setColor(Color.WHITE);
		cursorFontPaint.setTextSize(cursor_textSize);

		arcLineFontPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
		arcLineFontPaint.setColor(Color.WHITE);
		arcLineFontPaint.setTextSize(arc_textSize);
	}

	/**
	 * 初始化用到的各种图标和画笔
	 */
	private void init(Context contexts) {
//		gpsUtil = GpsUtil.getInstance(contexts);
		gpsUtil = LocationUtil.getInstance(contexts);
		float density = ScreenUtil.getScreenDensity(contexts);
		initBitmap();

		initDistance(contexts);

		touch_offset_Y = contexts.getResources().getDimension(
				R.dimen.cursor_touch_offset_Y)
				/ density;
		image_offset = contexts.getResources().getDimension(
				R.dimen.image_offset)
				/ density;
		RADIUS = contexts.getResources().getDimension(R.dimen.RADIUS) / density;

		image_bottom_offset = contexts.getResources().getDimension(
				R.dimen.image_bottom_offset)
				/ density;

		cursor_textSize = contexts.getResources().getDimension(
				R.dimen.cursor_textSize);
		mark_textSize = contexts.getResources().getDimension(
				R.dimen.mark_textSize);
		arc_textSize = contexts.getResources().getDimension(
				R.dimen.arc_textSize);
		green_textSize = contexts.getResources().getDimension(
				R.dimen.green_textSize);
		buttonY = 10 + image_bottom_offset;

		initPaint();

	}

	/**
	 * 初始化距离单位
	 */
	public void initDistance(Context contexts) {
		index = RountContexts.shortDistanceIndex;
		distanceUnit_near = DistanceUtil.getDistanceUnit(contexts, index);
		distanceArc = DistanceDataUtil.getDistanceArc(contexts);

		if (index == DistanceUtil.YARD) {
			oneDistanceToPixel /= DistanceUtil.RATE_YARD;
		}
	}

	public void setNavigationMapModel(MapModel navigationMapModel,
			Handler handler) {
		this.mapModel = navigationMapModel;
		this.handler = handler;
		util = (CoordinateUtil) navigationMapModel.getCoordinateUtil();
		oneDistanceToPixel = util.getOneMeterToPixel();
	}

	public MapModel getNavigationMapModel() {
		return mapModel;
	}

	/**
	 * @param context
	 */
	public AssistView(Context context) {
		super(context);
		init(context);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);
		if (mapModel == null || util == null) {
			canvas.drawColor(Color.BLACK);
			return;
		}
		if (util.getViewWidth() == 0) {
			util.setViewWidth(this.getMeasuredWidth());
			util.setViewHeight(this.getMeasuredHeight());
		}
		// 画小人
		float left_c = 0;
		float top_c = 0;

		left_c = mapModel.getPersonPoint().x;
		top_c = mapModel.getPersonPoint().y;

		// 画果岭小旗
		if (mapModel.getGreenPointArray().size() == 1) {
			Point p = mapModel.getGreenPointArray().get(0);
			String myLenToGreen = mapModel.getDistanceToGreen(p, index)
					+ distanceUnit_near;
			canvas.drawBitmap(flagBmp, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP,
					defaultPaint);
			canvas.drawText(myLenToGreen,
					LEFT_BITMAP_LEFT + flagBmp.getWidth(), LEFT_FlAG_TOP
							+ flagBmp.getHeight(), greenFontPaint);
		} else {
			int green_count = 0;
			for (Point p : mapModel.getGreenPointArray()) {
				green_count++;
				String myLenToGreen = mapModel.getDistanceToGreen(p, index)
						+ distanceUnit_near;
				if (green_count == 1) {
					canvas.drawBitmap(flagBmp1, LEFT_BITMAP_LEFT,
							LEFT_FlAG_TOP, defaultPaint);
					canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT * 2
							+ flagBmp1.getWidth(),
							LEFT_FlAG_TOP + flagBmp1.getHeight(),
							greenFontPaint);
				} else {
					canvas.drawBitmap(flagBmp2, LEFT_BITMAP_LEFT, LEFT_FlAG_TOP*2
							+ flagBmp2.getHeight(), defaultPaint);
					canvas.drawText(myLenToGreen, LEFT_BITMAP_LEFT * 2
							+ flagBmp2.getWidth(),
							2*LEFT_FlAG_TOP + flagBmp2.getHeight() * 2,
							greenFontPaint);
				}
			}
		}

		// 在航拍图上画弧线，及到每条弧线的长度
		if (distanceArc != null) {
			drawArcAndDistance(canvas, left_c, top_c);
		}

	}

	private boolean isPersonPosition() {
		return mapModel.isPersonPositionByLonLat(gpsUtil.longitude,
				gpsUtil.latitude);
	}

	/**
	 * 在航拍图上画弧线，及到每条弧线的长度
	 * 
	 * @param canvas
	 * @param left_c
	 * @param top_c
	 */
	private void drawArcAndDistance(Canvas canvas, float left_c, float top_c) {
		// 以小人位置为基点，显示“距离信息”时的偏移角度(使用时需要转换为弧度：*Math.PI/180)
		final int defaultSweepAngle = 90;
		final int defaultStartAngle = 225;
		final int textWidth = 120;

		float textSweepAngle = defaultStartAngle - 180;

		Path path = new Path();
		// 第一道弧线和距离信息
		float maxRadius = distanceArc[distanceArc.length - 1]
				* oneDistanceToPixel;

		float marginToRight = RightTriangleUtil.getNearSideLenth(maxRadius,
				textSweepAngle); // 临角边边长

		// 第一次获取“距离信息”在屏幕中弧线上的点位，判断是否超出屏幕范围，如果超出，则修改默认的60度为实际需要的大小
		Log.v(TAG, "maxRadius" + maxRadius);
		Log.v(TAG, "marginToRight:" + marginToRight);
		Log.v(TAG, "left_c" + left_c);
		if (marginToRight + textWidth + left_c > ScreenUtil.getScreenWidth(this
				.getContext())) {
			marginToRight = ScreenUtil.getScreenWidth(this.getContext())
					- left_c - textWidth;
			textSweepAngle = RightTriangleUtil.getAcuteAngle(marginToRight,
					maxRadius); // 反算出所需要的角度
			Log.v(TAG, "angle:" + textSweepAngle);
		}
		// 用于计算出外切于圆弧的直线上的两个点的辅助线
		float auxiliaryLineY = RightTriangleUtil.getOppositeSideLenth(
				textWidth, 90 - textSweepAngle); // 辅助线对边边长
		float auxiliaryLineX = RightTriangleUtil.getNearSideLenth(textWidth,
				90 - textSweepAngle); // 辅助线临角边边长

		for (int i = 0; i < distanceArc.length; i++) {
			float radius = distanceArc[i] * oneDistanceToPixel;
			if (radius < top_c) {
				RectF oval3 = new RectF(left_c - radius, top_c - radius, left_c
						+ radius, top_c + radius);
				paint.setColor(this.getResources().getColor(R.color.arc));
				canvas.drawArc(oval3, defaultStartAngle, defaultSweepAngle,
						false, paint);

				// 画弧
				float oppositeSide = RightTriangleUtil.getOppositeSideLenth(
						radius, textSweepAngle); // 对边边长
				float adjacentSide = RightTriangleUtil.getNearSideLenth(radius,
						textSweepAngle); // 临角边边长
				float distanceTextX = left_c + adjacentSide;
				float distanceTextY = top_c - oppositeSide;

				// 虚拟一条穿过这两个点同时外切该圆弧的直线（用于使显示的文字倾斜）
				path.moveTo(distanceTextX, distanceTextY);
				path.lineTo(distanceTextX + auxiliaryLineX, distanceTextY
						+ auxiliaryLineY);
				path.close();

				paint.setColor(this.getResources().getColor(R.color.white));
				canvas.drawTextOnPath(distanceArc[i] + "", path, 6, 6,
						arcLineFontPaint); // 沿着这条斜线画“距离信息”
				path.reset(); // 重置路径
			}
		}
	}

	public interface MapViewInterface {
		public void onclick();
	}

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		int measuredHeight = measureHeight(heightMeasureSpec);
		int measuredWidth = measureWidth(widthMeasureSpec);
		setMeasuredDimension(measuredWidth, measuredHeight);
	}

	private int measureHeight(int measureSpec) {
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		int result = 420;

		if (specMode == MeasureSpec.AT_MOST) {
			result = specSize;
		} else if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		}
		return result;
	}

	private int measureWidth(int measureSpec) {
		int specMode = MeasureSpec.getMode(measureSpec);
		int specSize = MeasureSpec.getSize(measureSpec);

		int result = 320;

		if (specMode == MeasureSpec.AT_MOST) {
			result = specSize;
		} else if (specMode == MeasureSpec.EXACTLY) {
			result = specSize;
		}
		return result;
	}

	/**
	 * true 触摸人
	 */
	private boolean isTouchMe = false;

	private boolean checkTouchMe(MotionEvent event) {
		float x = event.getX();
		float y = event.getY();

		if ((x < mapModel.getPersonPoint().x + image_offset)
				&& (x > mapModel.getPersonPoint().x - image_offset)
				&& (y > mapModel.getPersonPoint().y - image_offset)
				&& (y < mapModel.getPersonPoint().y + image_offset)
				&& !isPersonPosition()) {
			return true;
		}
		return false;
	}

	public boolean checkMarkMoveValid(float vx, float vy) {
		return checkMoveValid(vx, vy - (float) touch_offset_Y);
	}

	private boolean checkMoveValid(float vx, float vy) {
		Log.e(TAG, util.getViewWidth() + "," + util.getViewHeight());
		float x = vx;
		float y = vy;
		if (x < image_offset || y < image_offset
				|| x > (util.getViewWidth() - image_offset)
				|| y > (util.getViewHeight() - image_offset)) {
			/* check the outside of valid rectangle for view */
			return false;
		}

		return true;
	}

	public boolean onActionDown(MotionEvent event) {
		/* check pressed me or not */
		isTouchMe = checkTouchMe(event);
		if (isTouchMe) {
			return true;
		}

		float x, y;
		x = event.getX();
		y = event.getY();
		/* check function button */
		return true;
	}

	public boolean onActionMove(MotionEvent event) {
		if (mapModel != null) {
			if (isTouchMe) {
				// double personLonLat[] = util.getLonLatByTouch(event.getX(),
				// event.getY());
				mapModel.setPersonPoint(event.getX(), event.getY());
				handler.sendEmptyMessage(MapActivity.WHAT_LOCATION_REFRESH);
				return true;
			}
		}

		return false;
	}

	public boolean onActionUp(MotionEvent event) {
		if (isTouchMe) {
			isTouchMe = false;
			return true;
		}

		return false;
	}
}
