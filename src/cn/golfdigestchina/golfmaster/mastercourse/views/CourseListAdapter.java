package cn.golfdigestchina.golfmaster.mastercourse.views;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.List;

import com.baidu.location.w;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.CoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.utils.DistanceUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.GpsSwitchUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.LocationUtil;
/**
 * 
 * @author chengmingyan
 * 
 */
public class CourseListAdapter extends BaseAdapter{
	private static final String TAG = CourseListAdapter.class.getName();

	private LayoutInflater inflater;
	private Activity activity;
	private CourseListCellWrapper wrap;
	private List<CoursesObject> courseList;
	
	private LocationUtil location;

	private static final int MIN_COUNT = 0;
	
	public void setCoursesList(List<CoursesObject> coursesList) {
		this.courseList = coursesList;
	}

	public CourseListAdapter ( Activity activity)
	{
		this.activity = activity;
		inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		
		location = LocationUtil.getInstance(activity.getApplicationContext());
	}

	public int getCount ()
	{
		if(null == courseList || courseList.size() < MIN_COUNT){
			return MIN_COUNT;
		}
		
		return courseList.size();
	}
	
	public CoursesObject getItem ( int position )
	{
		if(null == courseList){
			return null;
		}
		if(position > courseList.size() - 1 || position < 0){
			return null;
		}
		return courseList.get(position);
	}
	
	public long getItemId ( int position )
	{
		return position;
	}

	public View getView ( int position, View convertView, ViewGroup parent )
	{
		if (convertView == null)
		{
			convertView = inflater.inflate(R.layout.course_item, null);
			wrap = new CourseListCellWrapper();
			wrap.courseName = (TextView) convertView.findViewById(R.id.tv_coursename);
			wrap.courseRange = (TextView) convertView.findViewById(R.id.tv_courseRange);
			wrap.ivCourseItem = (ImageView) convertView.findViewById(R.id.iv_course_item);
			convertView.setTag(wrap);
		}
		else
		{
			wrap = (CourseListCellWrapper) convertView.getTag();
		}
		
		CoursesObject courseBean = getItem(position);
		if(null ==  courseBean){
			convertView.setVisibility(View.INVISIBLE);
			return convertView;
		}else{
			convertView.setVisibility(View.VISIBLE);
		}
		
		wrap.courseName.setText(courseBean.getCoursesName() + activity.getString(R.string.golfcourse).toString());
		
		float distance;
		if( location.latitude==0 || location.longitude==0 ) {
			if( GpsSwitchUtil.ensureOpenGPS(this.activity) ) {
				wrap.courseRange.setText(R.string.positioning);
			} else {
				wrap.courseRange.setText(R.string.position_not);
			}
		} else {
			if (RountContexts.longDistanceIndex == DistanceUtil.MILE) {
				distance = DistanceUtil.getDistanceByMile(Double.parseDouble(courseBean.getLatitude()), Double.parseDouble(courseBean.getLongitude()), location.latitude, location.longitude);
			}else {
				distance = DistanceUtil.getDistanceByKilometer(Double.parseDouble(courseBean.getLatitude()), Double.parseDouble(courseBean.getLongitude()), location.latitude, location.longitude);
			}
			DecimalFormat df = new DecimalFormat("#.##"); 
			df.setRoundingMode(RoundingMode.HALF_UP);
			distance = Float.parseFloat(df.format(distance)+"");
			String typeUnit = DistanceUtil.getDistanceUnit(activity, RountContexts.longDistanceIndex);
			wrap.courseRange.setText(distance+typeUnit);
		}
		if( (position+1)%2==0 ) {
			convertView.setBackgroundResource(R.drawable.sl_course_item_white_gray);
			wrap.ivCourseItem.setImageResource(R.drawable.ic_course_item_white);
		} else {
			convertView.setBackgroundResource(R.drawable.sl_course_item_gray_white);
			wrap.ivCourseItem.setImageResource(R.drawable.ic_course_item_gray);
		}
		return convertView;
	}

	public static String getTag() {
		return TAG;
	}
	
}



