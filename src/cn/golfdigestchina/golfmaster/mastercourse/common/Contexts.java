package cn.golfdigestchina.golfmaster.mastercourse.common;

import android.os.Environment;

/**
 * @Project GolfMate
 * @package com.teewell.golfmate.common
 * @title Contexts.java
 * @Description 服务器请求地址（根据模块划分的）
 * @author DarkWarlords
 * @date 2013-1-21 下午1:42:27
 * @Copyright Copyright(C) 2013-1-21
 * @version 1.0.0
 */
public class Contexts {
	/**
	 * 网络接口模块常量------start
	 * 
	 * @author lihe
	 */
	public static class ServerURL {
		public static final String SERVER_URL = "http://app.teewell.cn/term/";
		/**
		 * term 中的 iface
		 */
		public static final String MODEL_IFACE = SERVER_URL + "iface/";
		/**
		 * term 中的 dataSync
		 */
		public static final String MODEL_DATA_SYNC = SERVER_URL + "dataSync/";
	}

	
	
	/**
	 * 网络接口模块常量------end
	 * 
	 * @author lihe
	 */
	/**
	 * SD卡根目录
	 */
	public static final String SD_PATH = Environment
			.getExternalStorageDirectory().getAbsoluteFile().getAbsolutePath() + "/";
	/**
	 * 手机数据库存储目录
	 */
	public static final String APPLICATION_PATH = ".masterCourse/";
	public static final String LOGO_PATH = SD_PATH + APPLICATION_PATH + "logo/";
	public static final String AERIALPHOTO_PATH = SD_PATH + APPLICATION_PATH + "aerialPhoto/";
	public static final String EXCEPTION_PATH = SD_PATH +  APPLICATION_PATH + "exception/";
	public static final String ALUMB_PATH_CONSTANTDS[] = {"dcim","DCIM","camera","Camera","CAMERA"};
	/**
	 * 系统常量------end
	 * 
	 * @author chengmingyan
	 */
	public static final int PHOTOHEIGHT = 200;
	public static float   mapRatio=(float)0.1;
	public static final String STRING_SEPARATOR = "<<!!!>>";//字符分隔符号
}