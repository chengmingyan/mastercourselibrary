package cn.golfdigestchina.golfmaster.mastercourse.models.course;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import cn.golfdigestchina.golfmaster.mastercourse.beans.AerialPhotoBean;
import cn.golfdigestchina.golfmaster.mastercourse.beans.CoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.DownloadAerialPhotoBean;
import cn.golfdigestchina.golfmaster.mastercourse.beans.FairwayObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.ProvinceBean;
import cn.golfdigestchina.golfmaster.mastercourse.beans.SubCoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.common.Contexts;
import cn.golfdigestchina.golfmaster.mastercourse.common.Contexts.ServerURL;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.costants.SharedPreferenceConstant;
import cn.golfdigestchina.golfmaster.mastercourse.models.ComparatorProvinceCity;
import cn.golfdigestchina.golfmaster.mastercourse.utils.AbstractAsyncResponseListener;
import cn.golfdigestchina.golfmaster.mastercourse.utils.AsyncHttpClient;
import cn.golfdigestchina.golfmaster.mastercourse.utils.Base64;
import cn.golfdigestchina.golfmaster.mastercourse.utils.ComparatorCourses;
import cn.golfdigestchina.golfmaster.mastercourse.utils.FileUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.MD5;
import cn.golfdigestchina.golfmaster.mastercourse.utils.PinYinUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.SharedPreferencesUtils;
import cn.golfdigestchina.golfmaster.mastercourse.views.CourseListAdapter;
import cn.golfdigestchina.golfmaster.mastercourse.views.CourseListCellWrapper;
import cn.golfdigestchina.golfmaster.mastercourse.views.XListView;

/**  
 * @Project GolfMate20130123
 * @package com.teewell.golfmate.models
 * @title CourseInfoModel.java 
 * @Description 球场数据模块
 * 功能：周边球场列表、全国球场列表、球场logo、球场天气
 * @author Administrator
 * @date 2013-1-24 下午5:11:27
 * @Copyright Copyright(C) 2013-1-24
 * @version 1.0.0
 */
@SuppressLint("SimpleDateFormat")
public class CourseInfoModel {
	private static final String TAG = CourseInfoModel.class.getName();
	private static final String REQUEST_LABEL = "request";
	private static final String REQUEST_LABEL_COURSEID = "courseID";
	private static final String REQUEST_LABEL_FEED_BACKCOURSEID = "courseId";
	private static final String REQUEST_LABEL_EVALUATE = "evaluate";
	private static final String REQUEST_LABEL_DATETIME = "dateTime";
	private static final String RESULT_LABEL_DATA = "data";
	private static final String RESULT_LABEL_LOGOURL = "logoURL";
	private static final String RESULT_LABEL_COURSES = "courses";
	private static final String RESULT_LABEL_COURSESINFO = "coursesInfo";
	private static final String RESULT_LABEL_RETURNCODE = "returnCode";
	private static final int SUCCESS_RETURNCODE = 1;
	private final static String REQUEST_LABEL_USERID = "userId";
	private final static String REQUEST_LABEL_MESSAGE = "message";
	private static final long MODIFYTIME_MIN = 0L;
	private static final String ACTION_COURSES = ServerURL.MODEL_IFACE + "obtianCourses.action";
	private static final String ACTION_SUBCOURSE = ServerURL.MODEL_IFACE + "obtianSubCourses.action";
	private static final String ACTION_COURSEIMAGES = ServerURL.MODEL_IFACE + "obtainCourseImagesByID.action";
	private static final String ACTION_COURSEINFO = ServerURL.MODEL_IFACE + "obtainCoursesInfoByID.action";
	
	private List<CoursesObject> courseList = null;

	public List<ProvinceBean> provincesBeans;

//	public static CourseInfoModel courseInfoModel = new CourseInfoModel();
	private Context applicationContext = null;
	private static CourseInfoModel INSTANCE;
	private Handler courseHandler;
	private SimpleDateFormat df = new SimpleDateFormat("MM-dd HH:mm");
	
	public static final int WHAT_SYNCDATA_SUCCEED = 201;
	public static final int WHAT_SYNCDATA_FAILED = -201;

	public static final int WHAT_GETSUBCOURSE_SUCCEED = 202;
	public static final int WHAT_GETSUBCOURSEINFO_SUCCEED = 203;
    public static final int WHAT_PROGRESS = 0x203;
    public static final int WHAT_FILD = -202;
    
	public static final int AROUND_COUNT = 20;
	
	private SharedPreferencesUtils sharedPreferencesUtils;
    private SharedPreferencesUtils getSharedPreferencesUtils(){
    	return sharedPreferencesUtils;
    }
	private CourseInfoModel(Context context){
		applicationContext = context.getApplicationContext();
		sharedPreferencesUtils = new SharedPreferencesUtils(applicationContext,SharedPreferenceConstant.SHAREDPREFERENCE_NAME);
	}
	public static CourseInfoModel getInstance(Context activity){
		if (null == INSTANCE) {
			INSTANCE = new CourseInfoModel(activity);
		}
		return INSTANCE;
	}
	
	private void cancelHandlerOperation(){
		if (courseHandler != null) {
			AsyncHttpClient.cancelRequest(courseHandler);
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CoursesObject> getCourseList()
	{
		if (courseList == null) {
			String response = getSharedPreferencesUtils().getString(SharedPreferenceConstant.COURSESINFO, null);
			if (response != null) {
				courseList = ((List<CoursesObject>) FileUtil.getBase64Object(response));
			}else{
				return null;
			}
		}
		
		return courseList;

	}
	
//	===================================================================================================
	public HttpPost getCoursesHttpPost(){
		HttpPost httpPost = null;
		long dateTime = getSharedPreferencesUtils().getLong(SharedPreferenceConstant.COURSE_MODIFYTIME, MODIFYTIME_MIN);
		List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_DATETIME, dateTime);
			list.add(new BasicNameValuePair(REQUEST_LABEL, object.toString()));
			String urlString =  appendUrlWithNoDateKeyAndHashCode(ACTION_COURSES, object.toString());
			System.out.println(urlString+object.toString());
			httpPost = new HttpPost(urlString);
			httpPost.setEntity(new UrlEncodedFormEntity(list,HTTP.UTF_8));
		} catch (JSONException e1) {
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return httpPost;
	}
	
	public void startSyncCourseList(Handler handler){
		cancelHandlerOperation();
		courseHandler = handler;
		AsyncHttpClient.sendRequest(handler, getCoursesHttpPost(), new AbstractAsyncResponseListener() {
			@SuppressWarnings("unchecked")
			@Override
			protected void onSuccess(String response) {
				try {
					int returnCode = new JSONObject(response).optInt(RESULT_LABEL_RETURNCODE);
					if (returnCode == SUCCESS_RETURNCODE) {
						JSONObject jsonObject;
						List<CoursesObject> coursesObjects = new ArrayList<CoursesObject>();
						long maxChangTime = getSharedPreferencesUtils().getLong(SharedPreferenceConstant.COURSE_MODIFYTIME, MODIFYTIME_MIN);
						jsonObject = new JSONObject(response).getJSONObject(RESULT_LABEL_DATA);
						String logoURL = jsonObject.optString(RESULT_LABEL_LOGOURL);
						JSONArray josArray = jsonObject.getJSONArray(RESULT_LABEL_COURSES);
						for (int i = 0; i < josArray.length(); i++) {
							CoursesObject object = CoursesObject.getCoursesObject((JSONObject)josArray.opt(i));
							if (object.getPastFlag()!=CoursesObject.VALID && maxChangTime==0) {
								continue;
							}
							coursesObjects.add(object);
						}
						//此处主要用于首次及后期球场更新操作（球场列表同步）
						if (coursesObjects.size()>0) {
							maxChangTime = coursesObjects.get(0).getModifyTime();
							for (int j = 0; j < coursesObjects.size(); j++) {
								long temp = coursesObjects.get(j).getModifyTime();
								if (maxChangTime<=temp) {
									maxChangTime = temp;
								}
							}
							List<CoursesObject> historyCoursesObjects = null;
								String info = getSharedPreferencesUtils().getString(SharedPreferenceConstant.COURSESINFO, null);
								if (info != null) {
									historyCoursesObjects = (List<CoursesObject>) FileUtil.getBase64Object(info);
									for (int i = 0; i < coursesObjects.size(); i++) {
										CoursesObject newCoursesObject = coursesObjects.get(i);
										boolean isExist = false;
										for (int j = 0; j < historyCoursesObjects.size(); j++) {
											CoursesObject oldCoursesObject = coursesObjects.get(j);
											if (newCoursesObject.getCouseID()==oldCoursesObject.getCouseID()) {
												if (newCoursesObject.getPastFlag()==CoursesObject.VALID) {
													historyCoursesObjects.set(j, newCoursesObject);
												}else {
													historyCoursesObjects.remove(j);
												}
												FileUtil.deleteFolderFile(Contexts.AERIALPHOTO_PATH+oldCoursesObject.getCouseID(), true);
												isExist = true;
												break;
											}
										}
										if (!isExist) {
											coursesObjects.add(newCoursesObject);
											historyCoursesObjects.add(newCoursesObject);
										}
									}
								}else {
									historyCoursesObjects = coursesObjects;
								}
								coursesObjects = historyCoursesObjects;
								getSharedPreferencesUtils().commitString(SharedPreferenceConstant.LOGOURL, logoURL);
								getSharedPreferencesUtils().commitLong(SharedPreferenceConstant.COURSE_MODIFYTIME, maxChangTime);
							String courseInfo = FileUtil.setBase64Object(historyCoursesObjects);
							Log.d(TAG, courseInfo);
							getSharedPreferencesUtils().commitString(SharedPreferenceConstant.COURSESINFO, courseInfo);
						}
						getSharedPreferencesUtils().commitString(SharedPreferenceConstant.COURSE_LAST_UPDATE_TIME, df.format(new Date()));
						courseHandler.sendEmptyMessage(WHAT_SYNCDATA_SUCCEED);
					}else {
						courseHandler.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
					}
				} catch (JSONException e) {
					courseHandler.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
//					e.printStackTrace();
				} catch (IOException e) {
					courseHandler.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
//					e.printStackTrace();
				}
			}
			@Override
			protected void onFailure(Throwable e) {
				Log.e(TAG, e.toString());
				courseHandler.sendEmptyMessage(WHAT_SYNCDATA_FAILED);
			}
		});
	}
	
//	===================================================================================================
	public HttpPost getSubCoursesHttpPost(int courseID){
		HttpPost httpPost = null;
		List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_COURSEID, courseID);
			list.add(new BasicNameValuePair(REQUEST_LABEL, object.toString()));
			String urlString =  appendUrlWithNoDateKeyAndHashCode(ACTION_SUBCOURSE, object.toString());
			Log.v(TAG, urlString);
			httpPost = new HttpPost(urlString);
			httpPost.setEntity(new UrlEncodedFormEntity(list,HTTP.UTF_8));
		} catch (JSONException e1) {
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return httpPost;
	}
	public void getSubCourseInfo(final CoursesObject coursesObject,Handler handler){
		cancelHandlerOperation();
		courseHandler = handler;
		if (coursesObject.getSubCourses()==null) {
			AsyncHttpClient.sendRequest(handler, getSubCoursesHttpPost(coursesObject.getCouseID()), new AbstractAsyncResponseListener() {
				@Override
				protected void onSuccess(final String response) {
					
					try {
						final JSONObject jsonObject = new JSONObject(response);
						int returnCode = jsonObject.optInt(RESULT_LABEL_RETURNCODE);
						if (returnCode == SUCCESS_RETURNCODE) {
							JSONObject data = jsonObject.getJSONObject(RESULT_LABEL_DATA);
//									String subCourseInfo = data.optString("subCourses");
							String resourceURL = data.optString("resourceURL");
							coursesObject.setResourceURL(resourceURL);
							ArrayList<SubCoursesObject> subCoursesObjects = new ArrayList<SubCoursesObject>();
							ArrayList<FairwayObject> fairwayObjects = new ArrayList<FairwayObject>();
							JSONArray subCourses = data.getJSONArray("subCourses");
							for (int i = 0; i < subCourses.length(); i++) {
								SubCoursesObject subCoursesObject = SubCoursesObject.jsonSubCourseObject(subCourses.optJSONObject(i));
								subCoursesObjects.add(subCoursesObject);
								fairwayObjects.addAll(subCoursesObject.getFairwayList());
							}

							List<AerialPhotoBean> list = RountContexts.getAerialPhotoBeanArray(fairwayObjects);
							DownloadAerialPhotoBean bean = new DownloadAerialPhotoBean();
							bean.setAerialPhotoBeanList(list);
							bean.setCoursesObject(coursesObject);
							bean.setResourceURL(resourceURL);
							bean.setSubCourses(subCoursesObjects);

							Message msg = new Message();
							msg.what = WHAT_GETSUBCOURSEINFO_SUCCEED;
							msg.obj = bean;
							msg.arg1 = 0;
							courseHandler.sendMessage(msg);
							
						}else {
							courseHandler.sendEmptyMessage(WHAT_FILD);
						}
					} catch (JSONException e) {
						e.printStackTrace();
					}
				}
				@Override
				protected void onFailure(Throwable e) {
					Log.e(TAG, e.toString());
					courseHandler.sendEmptyMessage(WHAT_FILD);
				}
			});
		}else {
			Message msg = new Message();
			msg.obj = coursesObject;
			msg.what = WHAT_GETSUBCOURSE_SUCCEED;
			courseHandler.sendMessage(msg);
		}
	}
	public void downloadAerialPhoto(DownloadAerialPhotoBean bean,int position){
		AerialPhotoBean aerialPhotoBean = bean.getAerialPhotoBeanList().get(position);
		String photoPath = Contexts.AERIALPHOTO_PATH+bean.getCoursesObject().getCouseID()+"/";
		String photoPathUrl = photoPath+aerialPhotoBean.getPhotoName()+"."+aerialPhotoBean.getPhotoMode();
		
		File fileUrl = new File(photoPath);
		if (!fileUrl.exists()) {
			fileUrl.mkdirs();
		}
		File file = new File(photoPathUrl);
		if (!file.exists()) {
			String url = bean.getResourceURL()+aerialPhotoBean.getPhotoPath()+aerialPhotoBean.getPhotoName()+"."+aerialPhotoBean.getPhotoMode();
			HttpGet httpRequest = new HttpGet(url);
			Message msg = new Message();
			msg.what = WHAT_PROGRESS;
			msg.arg1 = position+1;
//			msg.obj = bean;
			AerialPhotoResponseListener listener = new AerialPhotoResponseListener(photoPathUrl, courseHandler, msg);
			AsyncHttpClient.sendRequest(url, httpRequest, listener);
		}else {
			
			Message msg = new Message();
			msg.what = WHAT_PROGRESS;
			msg.arg1 = position+1;
			msg.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
//			msg.obj = bean;
			courseHandler.sendMessage(msg);
//			Log.i(TAG, photoPathUrl);
		}
	}

	private static final String USERINFO_DATEKEY_AND_HASH_CODE = "%1$s?t=%2$s&hash=%3$s";

public String appendUrlWithNoDateKeyAndHashCode(String urlString, String postString){
		
		String newUrl = null;
		//build timeStamp
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
		String timeStamp = dateFormat.format(new Date());
		try {
			StringBuffer postBuffer = new StringBuffer();
	
			//only for post request
			String postStringBase64 = Base64.encode(postString.getBytes("UTF-8"));
			postBuffer.append(postStringBase64);
			
			postBuffer.append(timeStamp);
			String hashMD5 = MD5.Encode16(postBuffer.toString());

			newUrl = String.format(USERINFO_DATEKEY_AND_HASH_CODE, urlString, timeStamp, hashMD5);
			
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace(System.err);
		}
		
		return newUrl;
	}
//	===================================================================================================

//	===================================================================================================
	public HttpPost getCourseImagesHttpPost(int courseID){
		HttpPost httpPost = null;
		List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_COURSEID, courseID);
			list.add(new BasicNameValuePair(REQUEST_LABEL, object.toString()));
			String urlString =  appendUrlWithNoDateKeyAndHashCode(ACTION_COURSEIMAGES, object.toString());
			System.out.println(urlString+object.toString());
			httpPost = new HttpPost(urlString);
			httpPost.setEntity(new UrlEncodedFormEntity(list,HTTP.UTF_8));
		} catch (JSONException e1) {
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return httpPost;
	}

//	===================================================================================================
	public HttpPost getCoursesInfoHttpPost(int courseID){
		HttpPost httpPost = null;
		List<BasicNameValuePair> list = new ArrayList<BasicNameValuePair>();
		JSONObject object = new JSONObject();
		try {
			object.put(REQUEST_LABEL_COURSEID, courseID);
			list.add(new BasicNameValuePair(REQUEST_LABEL, object.toString()));
			String urlString =  appendUrlWithNoDateKeyAndHashCode(ACTION_COURSEINFO, object.toString());
			System.out.println(urlString+object.toString());
			httpPost = new HttpPost(urlString);
			httpPost.setEntity(new UrlEncodedFormEntity(list,HTTP.UTF_8));
		} catch (JSONException e1) {
			e1.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return httpPost;
	}
//	===================================================================================================

//	===================================================================================================
	public List<CoursesObject> searchAllCourseByProvinceCity(String key, List<ProvinceBean> provinceList 
			){
		List<CoursesObject> srcCourseObjects = getCourseList();

		if (srcCourseObjects==null) {
			return srcCourseObjects;
		}
		//construct the course object list
		List<CoursesObject> retCourseObjects = null;
		if ("".equals(key.trim())) {
			retCourseObjects = new ArrayList<CoursesObject>(srcCourseObjects);
		}else {
			retCourseObjects = new ArrayList<CoursesObject>();
			
			Iterator<CoursesObject> srcIt = srcCourseObjects.iterator();
			while(srcIt.hasNext()){
				CoursesObject course = (CoursesObject)srcIt.next();
				if(PinYinUtil.stringContainsKey(course.getCoursesName(), key)){
					retCourseObjects.add(course);
				}
			}
		}
		
		//sort by province and city
		Collections.sort(retCourseObjects, new ComparatorProvinceCity());
		
		//construct the map
		provinceList.clear();
		Iterator<CoursesObject> srcIt = retCourseObjects.iterator();
		int pos = 0;
		String province = "";
		while(srcIt.hasNext()){
			CoursesObject course = (CoursesObject)srcIt.next();
			if (!course.getProvinceName().equals(province)){
				province = course.getProvinceName();
				ProvinceBean bean = new ProvinceBean();
				bean.setPos(pos);
				bean.setProvinceName(province);
				
				provinceList.add(bean);
			}
			pos++;

		}
	
		return retCourseObjects;
	}
//	===================================================================================================
	
//	周边球场常用==================================================================================	
	/**本地是否存储有球场列表信息
	 * @return
	 * true  有  
	 * false 无
	 */
	public boolean checkData(){
		boolean isExist = false;
		String courseInfo = getSharedPreferencesUtils().getString(SharedPreferenceConstant.COURSESINFO, null);
		if (courseInfo != null && !"".equals(courseInfo)) {
			isExist = true;
		}
		return isExist;
	}
	
	/** 
	 * 根据关键字查询球场
	 * @return 
	 */
	public List<CoursesObject> searchAroundCourseList(double lon, double lat, String key){

		List<CoursesObject> srcList = getCourseList();
		if(null == srcList){
			return null;
		}
		
		ComparatorCourses comparator = new ComparatorCourses(lon, lat);
		
		List<CoursesObject> listAll = null;
		if ("".equals(key.trim())){
			listAll = srcList;
		}else{
			listAll = new ArrayList<CoursesObject>();
			
			Iterator<CoursesObject> srcIt = srcList.iterator();
			while(srcIt.hasNext()){
				CoursesObject course = (CoursesObject)srcIt.next();
				if(PinYinUtil.stringContainsKey(course.getCoursesName(), key)){
					listAll.add(course);
				}
			}
		}
		
		Collections.sort(listAll, comparator);
		return listAll;
	}
	
	/**
	 * 通过省份名称查询球场
	 * @return provinceName 省份名称
	 */
	public List<CoursesObject> searchCoursesByProvinces( double lon, double lat,String provinceName ) {
		List<CoursesObject> allCoursesObject = getCourseList();
		if( allCoursesObject==null ) {
			return null;
		}
		if( provinceName==null ) {
			if( lon==0 || lat==0  ) {
				Collections.sort(allCoursesObject, new ComparatorProvinceCity());
				return allCoursesObject;
			} else {
				Collections.sort( allCoursesObject, new ComparatorCourses(lon, lat) );
				provinceName = allCoursesObject.get(0).getProvinceName();
			}
		} else {
			Collections.sort( allCoursesObject, new ComparatorCourses(lon, lat) );
		}
		List<CoursesObject> coursesObjectList = new ArrayList<CoursesObject>();
		for ( CoursesObject bean : allCoursesObject ) {
			if( provinceName!=null && provinceName.equals(bean.getProvinceName()) )
				coursesObjectList.add(bean);
		}
		return coursesObjectList;
	}
	
	/**
	 * 获取省份列表
	 */
	public List<String> getProvinces() {
		List<CoursesObject> coursesObjectList = getCourseList();
		if( coursesObjectList==null ) {
			return null;
		}
		Collections.sort(coursesObjectList, new ComparatorProvinceCity());
		List<String> provinceList = new ArrayList<String>();
		String tempProvinces = "";
		for ( CoursesObject bean : coursesObjectList ) {
			String province = bean.getProvinceName();
			if( !tempProvinces.equals(province) ) {
				provinceList.add(province);
				tempProvinces = province;
			}
		}
		return provinceList;
	}
	
	// laod the network data
	public	synchronized void loadNetworkData(int firstVisibleItem,int visibleItemCount,CourseListAdapter adapter,XListView listView) {
			// check weather is synchronizing the course list
			// cancel all of the useless request
			final String wildCard = "W:";
			final String evaluateWildCard = "W:EV:";
			final String logoWildCard = "W:LO:";
			final String weatherImageWildCard = "W:WE";
			final String weatherInfoWildCard = "W:WI";

			AsyncHttpClient.cancelWildRequest(wildCard);
			
			for (int pos = firstVisibleItem; pos < firstVisibleItem
					+ visibleItemCount; pos++) {

				// there is one step charge
				CoursesObject course = adapter.getItem(pos - 1);
				if (null == course) {
					continue;
				}

				View cellView = listView.getChildAt(pos - firstVisibleItem);
				Log.i(TAG, "pos:" + pos);
				if (null == cellView) {
					Log.i(TAG, "pos:" + pos + " is null");
					continue;
				}

				CourseListCellWrapper cellWrapper = (CourseListCellWrapper) cellView
						.getTag();
				if (null == cellWrapper) {
					continue;
				}

				Double evaluate = course.getEvaluate();
				if (evaluate == null) {
				}

				String logoName = course.getImages();
				if (logoName != null && !"".equals(logoName)) {
					File file = new File(Contexts.LOGO_PATH + logoName);
					if (!file.exists()) {
						HttpPost request =  getCourseImagesHttpPost(course.getCouseID());
						CourseLogoImageResponseListener listener = new CourseLogoImageResponseListener(
								Contexts.LOGO_PATH + logoName, cellWrapper);
						String key = logoWildCard + course.getCouseID();
						AsyncHttpClient.sendRequest(key, request, listener);
					}
				}
			}
		}
}

