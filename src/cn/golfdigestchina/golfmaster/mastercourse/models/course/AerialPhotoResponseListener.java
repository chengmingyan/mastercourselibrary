package cn.golfdigestchina.golfmaster.mastercourse.models.course;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.http.HttpEntity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import cn.golfdigestchina.golfmaster.mastercourse.beans.AerialPhotoBean;
import cn.golfdigestchina.golfmaster.mastercourse.utils.AsyncResponseListener;

public class AerialPhotoResponseListener implements AsyncResponseListener{
	@SuppressWarnings("unused")
	private static final String TAG = AerialPhotoResponseListener.class.getSimpleName();
	private String photoPathUrl;
	private Handler handler;
	private Message message;
	public AerialPhotoResponseListener(String photoPathUrl,Handler handler,Message message){
		this.handler = handler; 
		this.message = message; 
		this.photoPathUrl = photoPathUrl;
	}
	
	@Override
	public void onResponseReceived(HttpEntity response) {
		// 获得一个输入流
		InputStream is;
		try {
			is = response.getContent();
				Bitmap bitmap = BitmapFactory.decodeStream(is);
			if (bitmap!=null) {
				
				FileOutputStream bos = new FileOutputStream(photoPathUrl);
				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
				bitmap.isRecycled();
				bos.flush();
				bos.close();
				message.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
			}
			is.close();
		} catch (IllegalStateException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			handler.sendMessage(message);
		}
	}
	@Override
	public void onResponseReceived(Throwable response) {
		handler.sendMessage(message);
	}


}
