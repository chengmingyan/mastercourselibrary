package cn.golfdigestchina.golfmaster.mastercourse.models;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.graphics.Bitmap;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.Point;
import cn.golfdigestchina.golfmaster.mastercourse.utils.CoordinateUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.DistanceUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.GpsUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.LocationUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.ScreenUtil;

/**
 * 航拍图的数据结构
 * 
 * 1,果岭一般不动同时保存经纬度及坐标系X/Y 3,人和marke保存经纬度，每次换算以经纬度为主 2，触摸点只存坐标系X/Y
 * 
 * @author chengmingyan
 * 
 */
public class MapModel {
	private static final String TAG = MapModel.class.getSimpleName();
	private Point personPoint; // 人的位置
	private List<Point> greenPointArray; // 果岭的位置
	private Point touchPoint; // 点击的位置
	private CoordinateUtil coordinateUtil;
	private Bitmap courseBitmap;
	private Context context;
	private int currentHoleId;

	public Point getPersonPoint() {
		return this.personPoint;
	}

	public void setPersonPoint(float touchX, float touchY) {
		this.personPoint = coordinateUtil.getLonLatByTouch(touchX, touchY);
	}

	/**
	 * * 关键点：计算距离其实主要是两点的经纬度计算
	 * 1,人的位置有两个地方决定：a,为获取到Gps时手指拖动，此时可以保存XY及经纬度；b,有Gps时刷新此时可以保存XY及经纬度 2，
	 * 
	 * @param lon
	 * @param lat
	 */
	public void setPersonPoint(double lon, double lat) {
		this.personPoint = coordinateUtil.convertLonLatToOldXY(lon, lat);
	}

	public List<Point> getGreenPointArray() {
		if (greenPointArray == null) {
			greenPointArray = new ArrayList<Point>();
		}
		return this.greenPointArray;
	}

	public void addGreenPoint(double lon, double lat) {
		Point green = coordinateUtil.convertLonLatToOldXY(lon, lat);
		this.greenPointArray.add(green);
	}

	public Point getTouchPoint() {
		return this.touchPoint;
	}

	public void setTouchPoint(float touchX, float touchY) {
		this.touchPoint = coordinateUtil.getLonLatByTouch(touchX, touchY);
		System.out.println("touchPoint.x:" + touchPoint.x + ",touchPoint.y"
				+ touchPoint.y + "??");
	}

	public CoordinateUtil getCoordinateUtil() {
		return this.coordinateUtil;
	}

	public void setCoordinateUtil(CoordinateUtil coordinateUtil) {
		this.coordinateUtil = coordinateUtil;
	}

	public int getCurrentHoleId() {
		return currentHoleId;
	}

	public void setCurrentHoleId(int currentHoleId) {
		this.currentHoleId = currentHoleId;
	}

	private static MapModel instance;

	public static MapModel getInstance(Context context) {
		if (instance == null) {
			instance = new MapModel(context);
		}
		return instance;
	}

	public MapModel(Context context) {
		this.context = context;
	}

	public void setCourseBitmap(Bitmap rebitmapOrg) {
		touchPoint = new Point(-100, -100);
		courseBitmap = rebitmapOrg;
		resetPersonPoint();
	}

	public void refreshPersonPoint(Context context) {
		// 画小人
//		LocationUtil  location = LocationUtil.getInstance(context);
		// if (isPersonPositionByLonLat(location.longitude, location.latitude))
		// {
		// setPersonPoint(location.longitude, location.latitude);
		// } else
		if (personPoint == null || personPoint.lat == 0) {
			float personX = (float) ScreenUtil.getScreenWidth(context) / 2;
			float personY = (float) (ScreenUtil.getScreenHeight(context) - context
					.getResources().getDimension(
							R.dimen.person_defult_height_offset));
			setPersonPoint(personX, personY);
		} else {
			setPersonPoint(personPoint.lon, personPoint.lat);
		}
	}

	public void resetPersonPoint() {
		personPoint = new Point();
		// float personX = (float)ScreenUtil.getScreenWidth(context) / 2;
		// float personY = (float)(ScreenUtil.getScreenHeight(context) -
		// context.getResources().getDimension(R.dimen.person_defult_height_offset));
		// personPoint.x = personX;
		// personPoint.y = personY;
	}

	public boolean isPersonPositionByLonLat(double longitude, double latitude) {
		return latitude > 0 && longitude > 0;
		// return getRealDistancePersonToGreen(latitude, longitude) < 1000 ?
		// true
		// : false;
	}

	public Bitmap getCourseBitmap() {
		return courseBitmap;
	}

	/**
	 * 计算点击的点到分别到果岭和人的距离
	 * 
	 * @param green
	 *            果岭的坐标
	 * @return 返回的数组中，元素1为到果岭的距离，元素2为到人的距离
	 */
	public String[] getDistanceTouchPointToGreenAndPerson(Point green, int index) {
		String[] distances = new String[2];
		float tempUtil = 1f;
		if (index == DistanceUtil.YARD) {
			tempUtil = 0.9144f;
		}
		distances[0] = Math.round((getDistanceBettwen(green, touchPoint))
				/ tempUtil)
				+ "";
		distances[1] = Math.round((getDistanceBettwen(personPoint, touchPoint))
				/ tempUtil)
				+ "";
		return distances;
	}

	/**
	 * 计算人到果林的距离
	 * 
	 * @param green
	 *            果岭的坐标
	 * @return 返回的数组中，元素1为到果岭的距离，元素2为到人的距离
	 */
	public int getDistanceToGreen(Point green, int distanceUnit) {
		float tempUtil = 1;
		if (DistanceUtil.YARD == distanceUnit) {
			tempUtil = 0.9144f;
		}
		int len = Math.round((getDistanceBettwen(getPersonPoint(), green))
				/ tempUtil);
		return len;
	}

	public float getDistancePersonToGreen(Point green, int distanceUnit) {
		float tempUtil = 1;
		if (DistanceUtil.YARD == distanceUnit) {
			tempUtil = 0.9144f;
		}
		float len = Math.abs((getDistanceBettwen(getPersonPoint(), green))
				/ tempUtil);
		return len;
	}

	/**
	 * @param latitude
	 * @param longitude
	 * @return
	 */
	public float getRealDistancePersonToGreen(double latitude, double longitude) {
		Point p = getGreenPointArray().get(0);
		// double[] touchLonLat = coordinateUtil.getLonLatByTouch(p.x, p.y);
		return DistanceUtil.distanceBetween(latitude, longitude, p.lat, p.lon);
	}

	public enum Tee {
		BLUE, RED, WHITE, BLACK, GOLD
	}

	/**
	 * 计算两点之间的大地距离
	 * 
	 * @see com.util.Coordinates#getDistanceBettwen(com.dna.map.Point,
	 *      com.dna.map.Point)
	 */
	public float getDistanceBettwen(Point pointA, Point pointB) {
		// double[] touchLonLatA = coordinateUtil.getLonLatByTouch(pointA.x,
		// pointA.y);
		// double[] touchLonLatB = coordinateUtil.getLonLatByTouch(pointB.x,
		// pointB.y);
		// return DistanceUtil.distanceBetween(touchLonLatA[1], touchLonLatA[0],
		// touchLonLatB[1], touchLonLatB[0]);
		return DistanceUtil.distanceBetween(pointA.lat, pointA.lon, pointB.lat,
				pointB.lon);
	}

	public void refreshData(Context context) {
		// if (voList != null) {
		// setMapMarkeVoList(voList);
		// }
		refreshPersonPoint(context);
	}
}
