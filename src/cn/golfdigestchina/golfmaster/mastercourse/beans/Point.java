/*
 * Copyright (C), 2010, Teewell Network. All Rights Reserved
 * File name: Point.java
 * Project Name:Map
 * Author:Antony        Version:0.1          Date:Aug 31, 2010
 * History: 
 *     1.Antony create source code
       2...
 */
package cn.golfdigestchina.golfmaster.mastercourse.beans;

import android.graphics.PointF;

/**
 * ����ͼ���õ��ĵ����ݽṹ
 * @author Antony
 *
 */
public class Point extends PointF {
    public double lat;
    public double lon;
    
    public Point(){
        super();
    }
    
    public Point(float x, float y){
        super(x,y);
    }
    
    public Point(android.graphics.Point point){
        super(point);
    }
}
