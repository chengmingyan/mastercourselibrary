/**
 * 
 */
package cn.golfdigestchina.golfmaster.mastercourse.beans;

import java.io.Serializable;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * @Description
 * @Author 谭坚
 * @Title FairwayObject.java   
 * @Package cn.teewell.golf.data 
 * @Date 2012-5-10 下午1:56:28   
 * @Version 1.0  
 * Copyright: Copyright (c)  2012  
 * Company: teewell    
 */
public class FairwayObject implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer id;
//	private Integer coursesId;
//	private Integer subCoursesId;
//	private String teeLongitude;
//	private String teeLatitude;
	private Integer holdNo;
//	private Integer difficulty;
	private Integer poleNumber;
	private String greenFrontLongitude;
	private String greenFrontLatitude;
	private String greenBackLongitude;
	private String greenBackLatitude;
	private String greenLongitude;
	private String greenLatitude;
//	private String version;
//	private String remarks;
//	private String photoName;
//	private String luLongitude;
//	private String luLatitude;
//	private String rdLongitude;
//	private String rdLatitude;
//	private String photoPath;
//	private String photoMode;
//	private Float rotationView;
//	private long photoSize;
//	private String photoVersion;
//	private String coursesName;
//	private String subCoursesName;
	private AerialPhotoBean aerialPhotoBean;
	private ArrayList<RoadblockObject> roadblockList;
	/**
	 * 返回球道id
	 * @return
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * 设置球道id
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * 返回球场id
	 * @return
	 */
//	public Integer getCoursesId() {
//		return coursesId;
//	}
//	/**
//	 * 设置球场id
//	 * @param coursesId
//	 */
//	public void setCoursesId(Integer coursesId) {
//		this.coursesId = coursesId;
//	}
//	/**
//	 * 返回子球场id
//	 * @return
//	 */
//	public Integer getSubCoursesId() {
//		return subCoursesId;
//	}
//	/**
//	 * 设置子球场id
//	 * @param subCoursesId
//	 */
//	public void setSubCoursesId(Integer subCoursesId) {
//		this.subCoursesId = subCoursesId;
//	}
//	/**
//	 * 返回第一个T台经度
//	 * @return
//	 */
//	public String getTeeLongitude() {
//		return teeLongitude;
//	}
//	/**
//	 * 设置第一个T台经度
//	 * @param teeLongitude
//	 */
//	public void setTeeLongitude(String teeLongitude) {
//		this.teeLongitude = teeLongitude;
//	}
//	/**
//	 * 返回第一个T台纬度
//	 * @return
//	 */
//	public String getTeeLatitude() {
//		return teeLatitude;
//	}
//	/**
//	 * 设置第一个T台纬度
//	 * @param teeLatitude
//	 */
//	public void setTeeLatitude(String teeLatitude) {
//		this.teeLatitude = teeLatitude;
//	}
	/**
	 * 返回球洞编号
	 * @return
	 */
	public Integer getHoldNo() {
		return holdNo;
	}
	/**
	 * 设置球洞编号
	 * @param holdNo
	 */

	
	/**
	 * 返回球道难度系数
	 * @return
	 */
//	public Integer getDifficulty() {
//		return difficulty;
//	}
	public void setHoldNo(Integer holdNo) {
		this.holdNo = holdNo;
	}
	/**
	 * 设置球道难度系数
	 * @param difficulty
	 */
//	public void setDifficulty(Integer difficulty) {
//		this.difficulty = difficulty;
//	}
	/**
	 * 返回球道标准杆数
	 * @return
	 */
	public Integer getPoleNumber() {
		return poleNumber;
	}
	/**
	 * 设置球道标准杆数
	 * @param poleNumber
	 */
	public void setPoleNumber(Integer poleNumber) {
		this.poleNumber = poleNumber;
	}
	/**
	 * 返回果岭前沿经度
	 * @return
	 */
	public String getGreenFrontLongitude() {
		return greenFrontLongitude;
	}
	/**
	 * 设置球场前沿进度
	 * @param greenFrontLongitude
	 */
	public void setGreenFrontLongitude(String greenFrontLongitude) {
		this.greenFrontLongitude = greenFrontLongitude;
	}
	/**
	 * 返回果岭前沿纬度
	 * @return
	 */
	public String getGreenFrontLatitude() {
		return greenFrontLatitude;
	}
	/**
	 * 设置果岭前沿纬度
	 * @param greenFrontLatitude
	 */
	public void setGreenFrontLatitude(String greenFrontLatitude) {
		this.greenFrontLatitude = greenFrontLatitude;
	}
	/**
	 * 返回果岭后延经度
	 * @return
	 */
	public String getGreenBackLongitude() {
		return greenBackLongitude;
	}
	/**
	 * 设置果岭后延经度
	 * @param greenBackLongitude
	 */
	public void setGreenBackLongitude(String greenBackLongitude) {
		this.greenBackLongitude = greenBackLongitude;
	}
	/**
	 * 返回果岭后延纬度
	 * @return
	 */
	public String getGreenBackLatitude() {
		return greenBackLatitude;
	}
	/**
	 * 设置果岭后延纬度
	 * @param greenBackLatitude
	 */
	public void setGreenBackLatitude(String greenBackLatitude) {
		this.greenBackLatitude = greenBackLatitude;
	}
	/**
	 * 返回果岭中心经度
	 * @return
	 */
	public String getGreenLongitude() {
		return greenLongitude;
	}
	/**
	 * 设置果岭中心经度
	 * @param greenLongitude
	 */
	public void setGreenLongitude(String greenLongitude) {
		this.greenLongitude = greenLongitude;
	}
	/**
	 * 返回果岭中心纬度
	 * @return
	 */
	public String getGreenLatitude() {
		return greenLatitude;
	}
	/**
	 * 设置果岭中心纬度
	 * @param greenLatitude
	 */
	public void setGreenLatitude(String greenLatitude) {
		this.greenLatitude = greenLatitude;
	}
	/**
	 * 返回球道版本号
	 * @return
	 */
//	public String getVersion() {
//		return version;
//	}
//	/**
//	 * 设置球道版本号
//	 * @param version
//	 */
//	public void setVersion(String version) {
//		this.version = version;
//	}
//	/**
//	 * 返回球道备注信息
//	 * @return
//	 */
//	public String getRemarks() {
//		return remarks;
//	}
//	/**
//	 * 设置球道备注信息
//	 * @param remarks
//	 */
//	public void setRemarks(String remarks) {
//		this.remarks = remarks;
//	}
//	/**
//	 * 返回航拍图名
//	 * @return
//	 */
//	public String getPhotoName() {
//		return photoName;
//	}
//	/**
//	 * 设置航拍图名
//	 * @param photoName
//	 */
//	public void setPhotoName(String photoName) {
//		this.photoName = photoName;
//	}
//	/**
//	 * 返回航拍图左手经度
//	 * @return
//	 */
//	public String getLuLongitude() {
//		return luLongitude;
//	}
//	/**
//	 * 设置航拍图左手经度
//	 * @param luLongitude
//	 */
//	public void setLuLongitude(String luLongitude) {
//		this.luLongitude = luLongitude;
//	}
//	/**
//	 * 返回航拍图左手纬度
//	 * @return
//	 */
//	public String getLuLatitude() {
//		return luLatitude;
//	}
//	/**
//	 * 设置航拍图左手纬度
//	 * @param luLatitude
//	 */
//	public void setLuLatitude(String luLatitude) {
//		this.luLatitude = luLatitude;
//	}
//	/**
//	 * 返回航拍图右下经度
//	 * @return
//	 */
//	public String getRdLongitude() {
//		return rdLongitude;
//	}
//	/**
//	 * 设置航拍图右下经度
//	 * @param rdLongitude
//	 */
//	public void setRdLongitude(String rdLongitude) {
//		this.rdLongitude = rdLongitude;
//	}
//	/**
//	 * 返回航拍图右下纬度
//	 * @return
//	 */
//	public String getRdLatitude() {
//		return rdLatitude;
//	}
//	/**
//	 * 设置航拍图右下纬度
//	 * @param rdLatitude
//	 */
//	public void setRdLatitude(String rdLatitude) {
//		this.rdLatitude = rdLatitude;
//	}
//	/**
//	 * 返回航拍图路径
//	 * @return
//	 */
//	public String getPhotoPath() {
//		return photoPath;
//	}
//	/**
//	 * 设置航拍图路径
//	 * @param photoPath
//	 */
//	public void setPhotoPath(String photoPath) {
//		this.photoPath = photoPath;
//	}
//	/**
//	 * 返回航拍图存储格式
//	 * @return
//	 */
//	public String getPhotoMode() {
//		return photoMode;
//	}
//	/**
//	 * 设置航拍图存储格式
//	 * @param photoMode
//	 */
//	public void setPhotoMode(String photoMode) {
//		this.photoMode = photoMode;
//	}
//	/**
//	 * 返回航拍图旋转角度
//	 * @return
//	 */
//	public Float getRotationView() {
//		return rotationView;
//	}
//	/**
//	 * 设置航拍图旋转角度
//	 * @param rotationView
//	 */
//	public void setRotationView(Float rotationView) {
//		this.rotationView = rotationView;
//	}
//	/**
//	 * 返回航拍图大小
//	 * @return
//	 */
//	public long getPhotoSize() {
//		return photoSize;
//	}
//	/**
//	 * 设置航拍图大小
//	 * @param photoSize2
//	 */
//	public void setPhotoSize(long photoSize2) {
//		this.photoSize = photoSize2;
//	}
//	/**
//	 * 返回航拍图版本号
//	 * @return
//	 */
//	public String getPhotoVersion() {
//		return photoVersion;
//	}
//	/**
//	 * 设置航拍图版本号
//	 * @param photoVersion
//	 */
//	public void setPhotoVersion(String photoVersion) {
//		this.photoVersion = photoVersion;
//	}
//	/**
//	 * 返回球道所属球场名
//	 * @return
//	 */
//	public String getCoursesName() {
//		return coursesName;
//	}
//	/**
//	 * 设置球道所属球场名
//	 * @param coursesName
//	 */
//	public void setCoursesName(String coursesName) {
//		this.coursesName = coursesName;
//	}
//	/**
//	 * 返回球场所属子球场名
//	 *
//	 */
//	public String getSubCoursesName() {
//		return subCoursesName;
//	}
//	/**
//	 * 设置球场所属子球场名
//	 * @param subCoursesName
//	 */
//	public void setSubCoursesName(String subCoursesName) {
//		this.subCoursesName = subCoursesName;
//	}
	
	public AerialPhotoBean getAerialPhotoBean() {
		return aerialPhotoBean;
	}
	public void setAerialPhotoBean(AerialPhotoBean aerialPhotoBean) {
		this.aerialPhotoBean = aerialPhotoBean;
	}
	
	public ArrayList<RoadblockObject> getRoadblockList() {
		return roadblockList;
	}
	public void setRoadblockList(ArrayList<RoadblockObject> roadblockList) {
		this.roadblockList = roadblockList;
	}
	public static FairwayObject jsonFairwayObject(JSONObject jsonObject) {
		FairwayObject object = new FairwayObject();
		AerialPhotoBean aerialPhotoBean = AerialPhotoBean.jsonAerialPhotoBean(jsonObject.optJSONObject("aerialPhotoBean"));
		object.setAerialPhotoBean(aerialPhotoBean);
		object.setId(jsonObject.optInt("fairwayID"));
		object.setGreenBackLatitude(jsonObject.optString("greenBackLatitude"));
		object.setGreenBackLongitude(jsonObject.optString("greenBackLongitude"));
		object.setGreenFrontLatitude(jsonObject.optString("greenFrontLatitude"));
		object.setGreenFrontLongitude(jsonObject.optString("greenFrontLongitude"));
		object.setGreenLatitude(jsonObject.optString("greenLatitude"));
		object.setGreenLongitude(jsonObject.optString("greenLongitude"));
		object.setHoldNo(jsonObject.optInt("holdNo"));
		object.setPoleNumber(jsonObject.optInt("poleNumber"));
		ArrayList<RoadblockObject> roadblockObjects = new ArrayList<RoadblockObject>();
		JSONArray roadblockList = jsonObject.optJSONArray("roadblockList");
		for (int i = 0; i < roadblockList.length(); i++) {
			roadblockObjects.add(RoadblockObject.jsonRoadblockObject(roadblockList.optJSONObject(i)));
		}
		object.setRoadblockList(roadblockObjects);
		return object;
	}
	
}
