package cn.golfdigestchina.golfmaster.mastercourse.costants;

public class SharedPreferenceConstant {
	//用户模块
	public static final String CURRENT_USER_SHAREDPREFERENCE_NAME = "current_user_sharedPreference_name";
	public static final String DEFAULT_USER_SHAREDPREFERENCE_NAME = "default_user_sharedPreference_name";
	public static final String SHAREDPREFERENCE_NAME = "sharedPreference_name";//全局的存储，如存放球场模块信息、用户文件名信息
	public static final String SHAREDPREFERENCE_GOLFEROBJECT = "golferObject";
	public static final String SHAREDPREFERENCE_PLAYGAMEOBJECT = "playGameObject";
	
	
//	球场模块
	public static final String COURSE_LAST_UPDATE_TIME = "course_last_update_time";
	public final static String COURSESINFO = "coursesInfo";
	public final static String COURSE_MODIFYTIME = "modifyTime";
	public final static String LOGOURL = "logoURL";
	public static final String SCORECARD_LAST_UPDATE_TIME = "scorecard_last_update_time";
	public static final String SCOREBEAN = "scoreBean";
	public static final String USERBEAN = "userBean";
	
	public static final String SETTING_GPS_SWITCH = "gpsSwitch";
	public static final String SCREEN_OFF_TIMEOUT = "screen_off_timeout";
	public static final String GUIDE_JSON = "guide_json";
	
	public static final String GUIDE_DATA = "[{'id':1,'name':'1211.jpg','url':'http://121.199.51.212/apps/images/1211.jpg'},{'id':2,'name':'1212.jpg','url':'http://121.199.51.212/apps/images/1212.jpg'},{'id':3,'name':'1213.jpg','url':'http://121.199.51.212/apps/images/1213.jpg'}]";
	public static final String STRING_SPLIT = "_";
	
	
}
