package cn.golfdigestchina.golfmaster.mastercourse.utils;

import org.apache.http.HttpEntity;

/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title OutputHolder.java 
 * @Description TODO
 * @author DarkWarlords
 * @date 2012-11-13 下午3:49:27
 * @Copyright Copyright(C) 2012-11-13
 * @version 1.0.0
 */
public class OutputHolder{
	private HttpEntity response;
	private Throwable exception;
	private AsyncResponseListener responseListener;
	
	public OutputHolder(HttpEntity response, AsyncResponseListener responseListener){
		this.response = response;
		this.responseListener = responseListener;
	}
	
	public OutputHolder(Throwable exception, AsyncResponseListener responseListener){
		this.exception = exception;
		this.responseListener = responseListener;
	}

	public HttpEntity getResponse() {
		return response;
	}

	public Throwable getException() {
		return exception;
	}
	
	public AsyncResponseListener getResponseListener() {
		return responseListener;
	}
	
}