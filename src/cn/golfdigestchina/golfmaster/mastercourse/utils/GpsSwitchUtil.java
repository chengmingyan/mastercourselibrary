package cn.golfdigestchina.golfmaster.mastercourse.utils;


import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.net.Uri;
import android.util.Log;
import cn.golfdigestchina.golfmaster.mastercourse.costants.SharedPreferenceConstant;


/**  
 * @Project GolfMate
 * @package com.teewell.golfmate.utils
 * @title GpsUtil.java 
 * @Description TODO
 * @author Administrator
 * @date 2013-1-22 上午10:46:11
 * @Copyright Copyright(C) 2013-1-22
 * @version 1.0.0
 */
public class GpsSwitchUtil {
	/**
     * 确保GPS状态开启
     * @param mContext
     * @return boolean 开启返回true/否则返回false
     */
    public static boolean ensureOpenGPS(Context mContext) {
    	boolean flag = false;
        try {
            flag = checkGpsStatus(mContext);
            SharedPreferencesUtils util = new SharedPreferencesUtils(mContext);
            util.commitBoolean(SharedPreferenceConstant.SETTING_GPS_SWITCH, flag);
            Log.e("/////", flag+"");
            if(!flag){
            	flag = trigerSwitch(mContext);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        
        return flag;
    }   
    private static boolean checkGpsStatus(Context mContext){
    	LocationManager alm = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
        return alm.isProviderEnabled(android.location.LocationManager.GPS_PROVIDER);
    }
    /**
     * 切换GPS开关
     * @param mContext
     * @return 无返回值
     */
    private static boolean trigerSwitch(Context mContext) {
    	Intent gpsIntent = new Intent();
		gpsIntent.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
		gpsIntent.addCategory("android.intent.category.ALTERNATIVE");
		gpsIntent.setData(Uri.parse("custom:3"));
		try {
		    PendingIntent.getBroadcast(mContext, 0, gpsIntent, 0).send();
		    return checkGpsStatus(mContext);
		} catch (CanceledException e) {
		    e.printStackTrace();
		    return false;
		}
    }

    /**
     * 恢复GPS开关
     * @param mContext
     * @return 无返回值
     */
    public static void restoreSwitch(Context mContext) {
        SharedPreferencesUtils util = new SharedPreferencesUtils(mContext);
        boolean flag = util.getBoolean(SharedPreferenceConstant.SETTING_GPS_SWITCH, true);
        if(!flag){
        	trigerSwitch(mContext);
        }

    }
    
    /**
     * 跳转到系统GPS设置界面
     * @param mContext
     * @return 无返回值
     */
    public static void startGPS(Context mContext) {
        Intent fireAlarm = new Intent(
                "android.settings.LOCATION_SOURCE_SETTINGS");
        fireAlarm.addCategory(Intent.CATEGORY_DEFAULT);
        mContext.startActivity(fireAlarm);
    }

}

