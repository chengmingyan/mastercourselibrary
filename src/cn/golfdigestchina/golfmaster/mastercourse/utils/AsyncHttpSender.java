package cn.golfdigestchina.golfmaster.mastercourse.utils;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpResponseException;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.BufferedHttpEntity;

import android.os.AsyncTask;
import android.util.Log;

/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title AsyncHttpSender.java 
 * @Description TODO
 * @author DarkWarlords
 * @date 2012-11-13 下午3:48:06
 * @Copyright Copyright(C) 2012-11-13
 * @version 1.0.0
 */
public class AsyncHttpSender extends AsyncTask<InputHolder, Void, OutputHolder> {
	private static final String TAG = AsyncHttpSender.class.getName();

	@Override
	protected OutputHolder doInBackground(InputHolder... params) {
		HttpEntity entity = null;
		InputHolder input = params[0];
		try {
			if(isCancelled()){
				Log.i(TAG, "AsyncHttpSender.onPostExecute(): isCancelled() is true");
				return null; //Canceled, do nothing
			}
			  if(input.getRequest()==null) {
		        	return new OutputHolder(
		        			new HttpResponseException(-1000021,"网络连接为空"),
		        			input.getResponseListener());
		        }
			HttpResponse response = AsyncHttpClient.getClient().execute((HttpUriRequest) input.getRequest());
			StatusLine status = response.getStatusLine();
			if(isCancelled()){
				Log.i(TAG, "AsyncHttpSender.onPostExecute(): isCancelled() is true");
				return null; //Canceled, do nothing
			}
	        if(status.getStatusCode() >= 300) {
	        	return new OutputHolder(
	        			new HttpResponseException(status.getStatusCode(), status.getReasonPhrase()),
	        			input.getResponseListener());
	        }
	        
			entity = response.getEntity();
//			Log.v(TAG, "isChunked:" + entity.isChunked());
            if(entity != null) {
            	try{
            		entity = new BufferedHttpEntity(entity);
            	}catch(Exception e){
            		Log.e(TAG, e.getMessage(), e);
            		//ignore?
            	}
            }		
		} catch (ClientProtocolException e) {
//			Log.e(TAG, e.getMessage(), e);
			return new OutputHolder(e, input.getResponseListener());
		} catch (IOException e) {
//			Log.e(TAG, e.getMessage(), e);
			return new OutputHolder(e, input.getResponseListener());
		}
		return new OutputHolder(entity, input.getResponseListener());
	}
	
	
	@Override
	protected void onPostExecute(OutputHolder result) {
		super.onPostExecute(result);
		if(isCancelled()){
			Log.i(TAG, "AsyncHttpSender.onPostExecute(): isCancelled() is true");
			return ; //Canceled, do nothing
		}
		AsyncResponseListener listener = result.getResponseListener();
		HttpEntity response = result.getResponse();
		Throwable exception = result.getException();
		if(response!=null){
			listener.onResponseReceived(response);
		}else{
			Log.i(TAG, "AsyncHttpSender.onResponseReceived(exception)");
			listener.onResponseReceived(exception);
		}
	}
	
	@Override
    protected void onCancelled(){
		Log.i(TAG, "AsyncHttpSender.onCancelled()");
		super.onCancelled();
		//this.isCancelled = true;
	}
}
