package cn.golfdigestchina.golfmaster.mastercourse.utils;

import java.util.ArrayList;

public class PinYinUtil {

	public static String getPinYin(String input) {
		ArrayList<HanziToPinyin.Token> tokens = HanziToPinyin.getInstance().get(input);
		StringBuilder sb = new StringBuilder();
		if (tokens != null && tokens.size() > 0) {
			for (HanziToPinyin.Token token : tokens) {
				if (HanziToPinyin.Token.PINYIN == token.type) {
					sb.append(token.target);
				} else {
					sb.append(token.source);
				}
			}
		}
		return sb.toString().toUpperCase();
	}
	public static boolean stringContainsKey(String stringObject,String key){
		boolean flag = (key.getBytes().length == key.length())?false:true;
		if (flag) {
			if(stringObject.indexOf(key)>=0){
				return true;
			}
		}else {
			if(getPinYin(stringObject).indexOf(getPinYin(key)) >= 0){
				return true;
			}
		}
		return false;
	}
}
