package cn.golfdigestchina.golfmaster.mastercourse.utils;
import java.util.Comparator;

import cn.golfdigestchina.golfmaster.mastercourse.beans.CoursesObject;
public class ComparatorCourses implements Comparator<CoursesObject>{
	
	private double lon, lat;
	
	public ComparatorCourses(double lon, double lat)
	{
		this.lon = lon;
		this.lat = lat;
	}
	
	@Override
	public int compare(CoursesObject object1, CoursesObject object2) {
		float distance1 = DistanceUtil.distanceBetween(lat, lon, 
									Double.parseDouble(object1.getLatitude()), 
									Double.parseDouble(object1.getLongitude()));
		float distance2 = DistanceUtil.distanceBetween(lat, lon, 
				Double.parseDouble(object2.getLatitude()), 
				Double.parseDouble(object2.getLongitude()));
		
		if(distance1 > distance2){
			return 1;
		}else if (distance1 < distance2){
			return -1;
		}else{ 
			return 0;
		}
	}
 
}

