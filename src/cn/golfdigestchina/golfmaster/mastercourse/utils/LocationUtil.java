package cn.golfdigestchina.golfmaster.mastercourse.utils;

import com.baidu.location.*;
import com.baidu.location.LocationClientOption.LocationMode;

import android.content.Context;
import android.util.Log;
import android.os.Handler;
import android.os.Message;

public class LocationUtil {

	public static String TAG = LocationUtil.class.getName();
	private static final String COORTYPE = "gcj02";
	
	private LocationClient locationClient;
	
	public double latitude;
	public double longitude;
	
	private LocationMode locationMode = LocationMode.Hight_Accuracy;
	private RefreshLocationListener listener;
	public static LocationUtil gLocationUtil = null;
	private BDLocationListener myListener = new MyLocationListenner();
	private final static  int TIME_PEROID = 1000;
	public static synchronized LocationUtil getInstance(Context context){
		if(null == gLocationUtil){
			gLocationUtil = new LocationUtil(context.getApplicationContext()); 
		}
		return gLocationUtil;
	}
	
	private LocationUtil(Context context){
		locationClient = new LocationClient(context);
		locationClient.registerLocationListener(myListener);
		LocationClientOption option = new LocationClientOption();
		option.setOpenGps(true);			//打开gps
		option.setCoorType(COORTYPE);		//设置坐标类型
	    option.setScanSpan(TIME_PEROID );//系统默认最少是一秒
		option.setLocationMode(locationMode);        //不设置，默认是gps优先,设备与wifie混合使用
		option.setIsNeedAddress(true);
		locationClient.setLocOption(option);
	}
				
	/**
	 * 监听函数，有新位置的时候，刷新数据
	 */
	public class MyLocationListenner implements BDLocationListener {
		@Override
		public void onReceiveLocation(BDLocation location) {
			if (location == null)
				return ;
			double newLatitude = location.getLatitude();
			double newLongitude = location.getLongitude();
			longitude = newLongitude;
			latitude = newLatitude;
			Log.i(TAG, "get new location");
			if (listener != null) {
				if (location.getLocType() == BDLocation.TypeGpsLocation){//手机定位
					listener.refreshLocation(location);
				} else if (location.getLocType() == BDLocation.TypeNetWorkLocation){//网络定位
					listener.refreshLocation(location);
				}
			}
		}
		
		/* 获取到详细地址时
		 * @see com.baidu.location.BDLocationListener#onReceivePoi(com.baidu.location.BDLocation)
		 */
		public void onReceivePoi(BDLocation poiLocation) {
		}
	}
	
	public void setListener(RefreshLocationListener listener){
		locationClient.start();
		this.listener = listener;
	}
public interface RefreshLocationListener{
	public void refreshLocation(BDLocation location);
}
	public boolean refreshLocation()
	{
		if(locationClient != null && locationClient.isStarted()){
			locationClient.requestLocation();
			return true;
		}else{
			return false;
		}
	}
	
	public void clearListener(){
		listener = null;
		locationClient.stop();
	}

	public void destroy(){
		listener = null;
		if (locationClient.isStarted()) {
			locationClient.stop();
		}
		locationClient.unRegisterLocationListener(myListener);
		locationClient = null;
		gLocationUtil = null;
	}
}