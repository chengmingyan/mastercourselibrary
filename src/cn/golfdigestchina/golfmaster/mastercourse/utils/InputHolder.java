package cn.golfdigestchina.golfmaster.mastercourse.utils;

import org.apache.http.HttpRequest;

/**  
 * @Project com.vehiclesApp
 * @package com.vehiclesApp.utils
 * @title InputHolder.java 
 * @Description TODO
 * @author DarkWarlords
 * @date 2012-11-13 下午3:49:01
 * @Copyright Copyright(C) 2012-11-13
 * @version 1.0.0
 */
public class InputHolder{
	private HttpRequest request;
	private AsyncResponseListener responseListener;
	
	public InputHolder(HttpRequest request, AsyncResponseListener responseListener){
		this.request = request;
		this.responseListener = responseListener;
	}
	
	public HttpRequest getRequest() {
		return request;
	}

	public AsyncResponseListener getResponseListener() {
		return responseListener;
	}
}