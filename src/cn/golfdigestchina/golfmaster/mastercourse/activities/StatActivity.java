package cn.golfdigestchina.golfmaster.mastercourse.activities;

import com.baidu.mobstat.StatService;

import android.app.Activity;
import android.util.Log;


public abstract class StatActivity extends Activity {

	public abstract String getPageName();

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		Log.e("stat", "starta" + getPageName());
		StatService.onPageStart(this, getPageName());
		StatService.onResume(this);
		super.onResume();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		StatService.onPause(this);
		Log.e("stat", "stopa" + getPageName());
		StatService.onPageEnd(this, getPageName());
		super.onPause();
	}
}
