package cn.golfdigestchina.golfmaster.mastercourse.activities;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.adapter.ProvinceAdapter;
import cn.golfdigestchina.golfmaster.mastercourse.models.course.CourseInfoModel;

/**
 * 城市列表
 */
public class ProvinceListActivity extends StatActivity implements
		OnClickListener, OnItemClickListener {
	@SuppressWarnings("unused")
	private static final String TAG = ProvinceListActivity.class.getName();

	/**
	 * 调用者用来接收所选城市的标识
	 */
	public static final int RESULT_CODE_FOR_PROVINCE = 0x0021;
	/**
	 * 返回数据的参数名称
	 */
	public static final String RESULT_PARAM_PROVINCE = "param_province";

	private ListView lvCity = null;
	private ProvinceAdapter adapter = null;

	private List<String> provinces = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.activity_city_list);
		super.onCreate(savedInstanceState);
		initElements();
		loadData();
	}

	/**
	 * 初始化
	 */
	private void initElements() {
		TextView tvTitle = (TextView) findViewById(R.id.tv_title);
		tvTitle.setText(R.string.sel_city);
		ImageButton btnBack = (ImageButton) findViewById(R.id.btn_left);
		btnBack.setOnClickListener(this);
		lvCity = (ListView) findViewById(R.id.lv_city);
		lvCity.setOnItemClickListener(this);
	}

	@Override
	public String getPageName() {
		// TODO Auto-generated method stub
		return getString(R.string.baidu_page_course_city);
	}

	private void loadData() {
		provinces = CourseInfoModel.getInstance(this).getProvinces();
		adapter = new ProvinceAdapter(ProvinceListActivity.this,
				CourseInfoModel.getInstance(this).getProvinces());
		lvCity.setAdapter(adapter);
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.btn_left) {
			finish();
		}
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent data = new Intent();
		data.putExtra(RESULT_PARAM_PROVINCE, provinces.get(position));
		setResult(RESULT_CODE_FOR_PROVINCE, data);
		finish();
	}
}
