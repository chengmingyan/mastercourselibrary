package cn.golfdigestchina.golfmaster.mastercourse.activities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.AerialPhotoBean;
import cn.golfdigestchina.golfmaster.mastercourse.beans.CoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.DownloadAerialPhotoBean;
import cn.golfdigestchina.golfmaster.mastercourse.beans.PlayGameObject;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.costants.KeyValuesConstant;
import cn.golfdigestchina.golfmaster.mastercourse.models.course.CourseInfoModel;
import cn.golfdigestchina.golfmaster.mastercourse.utils.AsyncHttpClient;
import cn.golfdigestchina.golfmaster.mastercourse.utils.LocationUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.LocationUtil.RefreshLocationListener;
import cn.golfdigestchina.golfmaster.mastercourse.views.CourseListAdapter;
import cn.golfdigestchina.golfmaster.mastercourse.views.LoadView;
import cn.golfdigestchina.golfmaster.mastercourse.views.LoadView.Status;
import cn.golfdigestchina.golfmaster.mastercourse.views.ProgressDialogTitleView;

import com.baidu.location.BDLocation;

/**
 * 周边球场 该界面要注意：下拉刷新容易出问题
 * 
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title SurroundingActivity.java
 * @Description TODO
 * @author chengmingyan
 * @date 2013-1-15 14:52:36
 * @Copyright Copyright(C) 2013-1-15
 * @version 1.0.0
 */
public class AroundActivity extends StatActivity implements OnClickListener,
		OnItemClickListener, RefreshLocationListener {
	private final String TAG = AroundActivity.class.getSimpleName();
	private LoadView loadView;
	private ListView lvCourse;
	private ImageButton btn_cancel; // 搜索框中的清除按钮
	private EditText text_search; //
	private CourseListAdapter adapter;
	private CourseInfoModel courseInfoModel;
	public static final int WHAT_GOTTEN_LOCATION = 102;
	public static final int WHAT_LOAD_COURSELIST = 104;
	public static final int WHAT_LOAD_NETDATA = 105;
	private static final int REFRESH_DELAY = 1000 * 2;

	private ProgressDialog mProgress;
	private CoursesObject coursesObject;
	private PlayGameObject playGameObject;
	private LocationUtil locationUtil;

	private TextView tvCurrentCity = null;

	/**
	 * 当前的省份
	 */
	private String mCurrentProvince = null;

	@Override
	public String getPageName() {
		// TODO Auto-generated method stub
		return getString(R.string.baidu_page_course_list);
	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.around);
		initView();
		reloadAllData();
		locationUtil = LocationUtil.getInstance(this);
		locationUtil.setListener(this);
	}

	void reloadAllData() {
		handler.sendEmptyMessageDelayed(WHAT_LOAD_COURSELIST, REFRESH_DELAY);
	}

	/**
	 * 进度条Dialog
	 */
	private void progressDialog(CoursesObject object) {
		mProgress = null;
		mProgress = new ProgressDialog(this);
		mProgress.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);

		mProgress.setButton(getString(R.string.cancel_aysncCourse),
				new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						AsyncHttpClient.cancelAllRequest();
					}
				});

		mProgress.setCustomTitle(new ProgressDialogTitleView(this, object)
				.getView());
		if (!mProgress.isShowing()) {
			mProgress.show();
			mProgress.setCancelable(false);
			mProgress.setProgress(0);
		}
	}

	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		coursesObject = adapter.getItem(position);

		if (coursesObject == null) {
			return;
		}
		if (playGameObject == null) {
			progressDialog(coursesObject);
			courseInfoModel.getSubCourseInfo(coursesObject, handler);
		} else {
			Dialog dialog = new AlertDialog.Builder(AroundActivity.this)
					.setMessage(R.string.play_hint)
					.setPositiveButton(R.string.ok,
							new DialogInterface.OnClickListener() {
								public void onClick(DialogInterface arg0,
										int arg1) {
									arg0.dismiss();
									RountContexts.saveRoundInfo(
											AroundActivity.this, null);
									progressDialog(coursesObject);
									courseInfoModel.getSubCourseInfo(
											coursesObject, handler);
								};
							})
					.setNegativeButton(R.string.cancel,
							new DialogInterface.OnClickListener() {
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									dialog.dismiss();
									Intent intent = new Intent(
											AroundActivity.this,
											MapActivity.class);
									// 如果已经打开过的实例，将不会重新打开新的Activity
									intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
									intent.putExtra(
											KeyValuesConstant.INTENT_PLAYGAMEOBJECT,
											playGameObject);
									startActivity(intent);
								}
							}).create();
			dialog.show();
		}
	}

	/**
	 * 初始化组件及事件
	 */
	private void initView() {
		tvCurrentCity = (TextView) findViewById(R.id.tv_current_city);
		btn_cancel = (ImageButton) findViewById(R.id.btn_cancel);
		btn_cancel.setOnClickListener(this);
		text_search = (EditText) findViewById(R.id.edit_search);
		text_search.addTextChangedListener(new TextWatcher() {
			public void afterTextChanged(Editable s) {
				if (s != null && s.length() > 0) {
					btn_cancel.setVisibility(View.VISIBLE);

				} else {
					btn_cancel.setVisibility(View.GONE);
				}

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {
			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {
				if ("".equals(s.toString())) {

				} else {

				}
				refreshDataByProvince();
			}
		});
		text_search.setOnTouchListener(new OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				switch (event.getAction()) {
				case MotionEvent.ACTION_UP:
					int curX = (int) event.getX();
					if (curX > v.getWidth() - 38
							&& !TextUtils.isEmpty(text_search.getText())) {
						text_search.setText("");
						int cacheInputType = text_search.getInputType();// backup
						text_search.setInputType(InputType.TYPE_NULL);// disable
						text_search.onTouchEvent(event);// call native handler
						text_search.setInputType(cacheInputType);// restore
																	// input
						return true;// consume touch even
					}
					break;
				}
				return false;
			}
		});
		text_search
				.setOnEditorActionListener(new EditText.OnEditorActionListener() {
					@Override
					public boolean onEditorAction(TextView v, int actionId,
							KeyEvent event) {
						if (actionId == EditorInfo.IME_ACTION_DONE) {
							InputMethodManager imm = (InputMethodManager) v
									.getContext().getSystemService(
											Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
							// 隐藏软键盘
							return true;
						}
						return false;
					}
				});

		loadView = (LoadView) this.findViewById(R.id.load_view);
		courseInfoModel = CourseInfoModel.getInstance(this);
		adapter = new CourseListAdapter(this);
		loadView.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				loadView.setStatus(Status.loading);
				reloadAllData();
			}
		});
		loadView.setStatus(Status.loading);
		lvCourse = (ListView) findViewById(R.id.lv_course);
		lvCourse.setAdapter(adapter);
		lvCourse.setVisibility(View.GONE);
		lvCourse.setOnItemClickListener(this);
	}

	public void onClick(View v) {
		if (R.id.btn_cancel == v.getId()) {
			text_search.setText("");
		} else if (R.id.btn_right == v.getId()) {
			dumpCityIntent();
		}
	}

	/**
	 * 跳转到城市列表
	 */
	private void dumpCityIntent() {
		Intent intent = new Intent(this, ProvinceListActivity.class);
		startActivityForResult(intent, 0);
	}

	private final Handler handler = new Handler() {
		private DownloadAerialPhotoBean downloadAerialPhotoBean;
		private int aerialPhotoCount;

		public void handleMessage(Message msg) {
			switch (msg.what) {
			case CourseInfoModel.WHAT_SYNCDATA_SUCCEED:
				if (!"".equals(text_search.getText().toString())) {
					text_search.setText("");
				}
				SimpleDateFormat format = new SimpleDateFormat(
						getString(R.string.date_format));
				Date now = new Date();

				refreshDataByProvince();
				break;
			case CourseInfoModel.WHAT_SYNCDATA_FAILED: {
				if (!"".equals(text_search.getText().toString())) {
					text_search.setText("");
				}
				Toast.makeText(AroundActivity.this, R.string.bad_network,
						Toast.LENGTH_SHORT).show();
				refreshDataByProvince();
			}
				break;

			case CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED:
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
					mProgress = null;
				}
				Intent intent = new Intent(AroundActivity.this,
						StartGameActivity.class);
				intent.putExtra(KeyValuesConstant.INTENT_COURSESOBJECT,
						(CoursesObject) msg.obj);
				RountContexts.fromActivity = 0;
				startActivity(intent);
				// 通过以下方法设置动画，第一个参数设置启动另一个Activity时的动画，第二个参数设置退出另一个Activity时的动画。值为0时表示什么都不做
				// 参数为int型，即动画XML的ID
				// overridePendingTransition(R.anim.zoom_enter,
				// R.anim.zoom_exit);
				break;
			case CourseInfoModel.WHAT_GETSUBCOURSEINFO_SUCCEED:
				downloadAerialPhotoBean = (DownloadAerialPhotoBean) msg.obj;
				aerialPhotoCount = downloadAerialPhotoBean
						.getAerialPhotoBeanList().size();
				mProgress.setMax(aerialPhotoCount);
				Message message = new Message();
				message.what = CourseInfoModel.WHAT_PROGRESS;
				message.arg1 = msg.arg1;
				message.arg2 = AerialPhotoBean.DOWNLOAD_SUCCESSED;
				handler.sendMessage(message);
				break;
			case WHAT_LOAD_COURSELIST: {
				courseInfoModel.startSyncCourseList(handler);
			}
				break;
			case CourseInfoModel.WHAT_FILD: {
				if (mProgress != null && mProgress.isShowing()) {
					mProgress.dismiss();
					mProgress = null;
				}
				Toast.makeText(AroundActivity.this, R.string.bad_network,
						Toast.LENGTH_SHORT).show();
			}
				break;
			case CourseInfoModel.WHAT_PROGRESS:
				if (mProgress != null && mProgress.isShowing()) {
					if (msg.arg2 != AerialPhotoBean.DOWNLOAD_SUCCESSED) {
						mProgress.dismiss();
						mProgress = null;
						Toast.makeText(AroundActivity.this,
								R.string.bad_network, Toast.LENGTH_SHORT)
								.show();
					} else {
						mProgress.setProgress(msg.arg1);
						if (msg.arg1 < aerialPhotoCount) {
							courseInfoModel.downloadAerialPhoto(
									downloadAerialPhotoBean, msg.arg1);
						} else {
							mProgress.dismiss();
							mProgress = null;
							CoursesObject coursesObject = downloadAerialPhotoBean
									.getCoursesObject();
							coursesObject.setSubCourses(downloadAerialPhotoBean
									.getSubCourses());
							Message message2 = new Message();
							message2.what = CourseInfoModel.WHAT_GETSUBCOURSE_SUCCEED;
							message2.obj = coursesObject;
							handler.sendMessage(message2);
						}
					}
				}
				break;
			default:
				break;
			}
		}
	};

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		Log.e(TAG, "restore");
		super.onRestoreInstanceState(savedInstanceState);
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		// TODO Auto-generated method stub
		Log.e(TAG, "save");
		super.onSaveInstanceState(outState);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		switch (resultCode) {
		// 选择城市后，刷新数据
		case ProvinceListActivity.RESULT_CODE_FOR_PROVINCE:
			if (data != null) {
				mCurrentProvince = data
						.getStringExtra(ProvinceListActivity.RESULT_PARAM_PROVINCE);
				tvCurrentCity.setText(mCurrentProvince);
				refreshDataByProvince();
			}
			break;
		default:
			break;
		}
	}

	@Override
	public void refreshLocation(BDLocation location) {
		locationUtil.clearListener();
		if (mCurrentProvince != null) {
			return;
		}
		mCurrentProvince = location.getProvince();
		tvCurrentCity.setText(mCurrentProvince);
		refreshDataByProvince();
	}

	private void refreshDataByProvince() {
		List<CoursesObject> coursesObjects = CourseInfoModel.getInstance(
				AroundActivity.this)
				.searchCoursesByProvinces(locationUtil.longitude,
						locationUtil.latitude, mCurrentProvince);
		if (coursesObjects == null || coursesObjects.size() <= 0) {
			loadView.setStatus(Status.error);
			lvCourse.setVisibility(View.GONE);
		} else {
			loadView.setStatus(Status.successed);
			lvCourse.setVisibility(View.VISIBLE);

			adapter.setCoursesList(coursesObjects);
			adapter.notifyDataSetChanged();
		}
	}
}
