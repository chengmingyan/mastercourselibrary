package cn.golfdigestchina.golfmaster.mastercourse.activities;

import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.CoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.PlayGameObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.SubCoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.costants.KeyValuesConstant;

public class StartGameActivity extends ParentActivity implements
		OnClickListener {
	private final String TAG = StartGameActivity.class.getName();
	private Button btnBegin;
	private TextView tvCourseName;
	private RelativeLayout layoutSubcourse1, layoutSubcourse2, layoutHalfpace,
			layoutHole, layoutSetting;
	private TextView tvSelect, tvSubcourse1, tvSubcourse2, tvHalfpace, tvHole;

	public static final int REQUESTCODE_SELECT = 0;

	private CoursesObject coursesObject;
	private PlayGameObject playGameObject;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_start_game);
		coursesObject = (CoursesObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_COURSESOBJECT);
		initView();
		initDate();
	}

	@Override
	public String getPageName() {
		// TODO Auto-generated method stub
		return getString(R.string.baidu_page_course_map);
	}

	private void initRountContexts() {
		playGameObject = new PlayGameObject();
		playGameObject.setCoursesObject(coursesObject);
		List<SubCoursesObject> subCoursesObjects = coursesObject
				.getSubCourses();
		RountContexts.curHoleNumber = 0;
		for (SubCoursesObject subCoursesObject : subCoursesObjects) {
			if (subCoursesObject.getFairwayCount() >= RountContexts.FAIRWAY_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
				break;
			}
		}
		if (playGameObject.getSubCourseNameArray()[0] == null) {
			if (subCoursesObjects.size() == RountContexts.SUBCOURSE_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObjects
						.get(0).getId();
				playGameObject.getSubCourseIdArray()[1] = subCoursesObjects
						.get(0).getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObjects
						.get(0).getSubCoursesName();
				playGameObject.getSubCourseNameArray()[1] = subCoursesObjects
						.get(0).getSubCoursesName();
			} else {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObjects
						.get(0).getId();
				playGameObject.getSubCourseIdArray()[1] = subCoursesObjects
						.get(1).getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObjects
						.get(0).getSubCoursesName();
				playGameObject.getSubCourseNameArray()[1] = subCoursesObjects
						.get(1).getSubCoursesName();
			}
		}
	}

	private void initDate() {

		tvCourseName.setText(getResources().getString(R.string.gameset));

		initRountContexts();

		initSetting();
	}

	private void initSetting() {
		tvHalfpace.setText(playGameObject.getStartTee());
		tvHole.setText((playGameObject.getStartHole() + 1) + "号洞");
		if (playGameObject.getSubCourseNameArray()[1] != null) {
			layoutSubcourse2.setVisibility(View.VISIBLE);

			tvSubcourse1.setText(playGameObject.getSubCourseNameArray()[0]
					+ "-( 9洞)");
			tvSubcourse2.setText(playGameObject.getSubCourseNameArray()[1]
					+ "-( 9洞)");
			tvSelect.setText(getString(R.string.select_frontnine_hole)
					.toString());
		} else {
			tvSelect.setText(getString(R.string.select_eighteen_hole)
					.toString());
			tvSubcourse1.setText(playGameObject.getSubCourseNameArray()[0]
					+ "-(18洞)");
			layoutSubcourse2.setVisibility(View.GONE);
		}
		if (tvSubcourse1.getText().length() > 10) {
			tvSubcourse1.setTextSize(14);
			tvSubcourse2.setTextSize(14);
		}
		RountContexts.asyncParAndAerialPhotoBeans(playGameObject);
	}

	private void initView() {
		tvCourseName = (TextView) findViewById(R.id.tv_title);
		layoutSubcourse1 = (RelativeLayout) findViewById(R.id.layout_subCourse1);
		layoutSubcourse2 = (RelativeLayout) findViewById(R.id.layout_subCourse2);
		tvSelect = (TextView) findViewById(R.id.tv_select);
		tvSubcourse1 = (TextView) findViewById(R.id.tv_subCourse1);
		tvSubcourse2 = (TextView) findViewById(R.id.tv_subCourse2);

		layoutHole = (RelativeLayout) findViewById(R.id.layout_hole);
		layoutHalfpace = (RelativeLayout) findViewById(R.id.layout_halfpace);
		layoutSetting = (RelativeLayout) findViewById(R.id.layout_setting);
		tvHalfpace = (TextView) findViewById(R.id.tv_halfpace);
		tvHole = (TextView) findViewById(R.id.tv_hole);
		btnBegin = (Button) findViewById(R.id.btn_begin);

		btnBegin.setOnClickListener(this);
		layoutHalfpace.setOnClickListener(this);
		layoutHole.setOnClickListener(this);
		layoutSubcourse1.setOnClickListener(this);
		layoutSubcourse2.setOnClickListener(this);
		layoutSetting.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (R.id.layout_subCourse1 == v.getId()) {
			doSelect(SelectActivity.SELECT_SUB_I);
		} else if (R.id.layout_subCourse2 == v.getId()) {
			doSelect(SelectActivity.SELECT_SUB_II);
		} else if (R.id.layout_halfpace == v.getId()) {
			doSelect(SelectActivity.SELECT_TEE);
		} else if (R.id.layout_hole == v.getId()) {
			doSelect(SelectActivity.SELECT_HOLE);
		} else if (R.id.btn_back == v.getId()) {
			finish();
		} else if (R.id.btn_begin == v.getId()) {
			startGame();
		} else if (R.id.layout_setting == v.getId()) {
			Intent intent = new Intent(StartGameActivity.this,
					CourseSettingActivity.class);
			startActivity(intent);
		}
	}

	private void doSelect(String type) {
		Intent i = new Intent(this, SelectActivity.class);
		i.putExtra(SelectActivity.SELECT_TYPE, type);
		i.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT, playGameObject);
		startActivityForResult(i, REQUESTCODE_SELECT);
	}

	private void startGame() {
		Intent intent = new Intent(this, MapActivity.class);
		intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT, playGameObject);
		// RountContexts.playGameObject = playGameObject;
		startActivity(intent);
		finish();
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		Log.d(TAG, "onKeyDown.....");
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			Log.d(TAG, "onKeyDown..... back is click");
			finish();
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == REQUESTCODE_SELECT && resultCode == RESULT_OK) {
			playGameObject = (PlayGameObject) data
					.getSerializableExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
			initSetting();
		}
	}
}
