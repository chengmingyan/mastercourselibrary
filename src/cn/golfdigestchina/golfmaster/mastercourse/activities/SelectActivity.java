package cn.golfdigestchina.golfmaster.mastercourse.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.PlayGameObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.SubCoursesObject;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.costants.KeyValuesConstant;
import cn.golfdigestchina.golfmaster.mastercourse.views.TextAdapter;

public class SelectActivity extends ParentActivity implements
		View.OnClickListener, OnItemClickListener {
	private TextView title;
	private ListView listView;
	private String selectType;
	private ArrayList<String> list;
	private TextAdapter adapter;
	private PlayGameObject playGameObject;
	public static final String SELECT_TYPE = "select_type";
	public static final String SELECT_SUB_I = "sub1";
	public static final String SELECT_SUB_II = "sub2";
	public static final String SELECT_TEE = "tee";
	public static final String SELECT_HOLE = "hole";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_select);

		initView();
		initDate();
	}

	@Override
	public String getPageName() {
		// TODO Auto-generated method stub
		return getString(R.string.baidu_page_course_setting);
	}

	private void initDate() {
		selectType = getIntent().getStringExtra(SELECT_TYPE);
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		if (selectType.equals(SELECT_TEE)) {
			list = getTeeList();
			title.setText(getString(R.string.select_T));
			adapter = new TextAdapter(this, list, playGameObject.getStartTee());
		} else if (selectType.equals(SELECT_HOLE)) {
			list = getHoleList();
			title.setText(getString(R.string.select_hole));
			adapter = new TextAdapter(this, list,
					(playGameObject.getStartHole() + 1) + "号洞");
		} else if (selectType.equals(SELECT_SUB_I)) {
			list = getSubCourseNameList();
			title.setText(getString(R.string.select_eighteen_hole));
			adapter = new TextAdapter(this, list,
					playGameObject.getSubCourseNameArray()[0]);
			RountContexts.isChangeSubCourse = true;
		} else {
			list = getSubCourseNameList();
			title.setText(getString(R.string.select_eighteen_hole));
			adapter = new TextAdapter(this, list,
					playGameObject.getSubCourseNameArray()[1]);
			RountContexts.isChangeSubCourse = true;
		}

		listView.setAdapter(adapter);

	}

	private ArrayList<String> getHoleList() {
		ArrayList<String> holeList = new ArrayList<String>();
		for (int i = 0; i < 18; i++) {
			holeList.add(Integer.toString(i + 1) + "号洞");
		}
		return holeList;
	}

	/**
	 * 获取梯台列表
	 * 
	 * @return ArrayList<String>
	 */
	private ArrayList<String> getTeeList() {
		ArrayList<String> teeList = new ArrayList<String>();
		teeList.add("金T");
		teeList.add("墨T");
		teeList.add("蓝T");
		teeList.add("白T");
		teeList.add("红T");
		return teeList;
	}

	private ArrayList<String> getSubCourseNameList() {
		ArrayList<String> nameList = new ArrayList<String>();
		for (int i = 0; i < playGameObject.getCoursesObject().getSubCourses()
				.size(); i++) {
			int count = playGameObject.getCoursesObject().getSubCourses()
					.get(i).getFairwayCount();
			if (count >= RountContexts.FAIRWAY_COUNT) {
				count = RountContexts.FAIRWAY_COUNT;
			} else {
				count = RountContexts.FAIRWAY_COUNT / 2;
			}
			nameList.add(playGameObject.getCoursesObject().getSubCourses()
					.get(i).getSubCoursesName()
					+ "-(" + count + "洞)");
		}
		return nameList;
	}

	private void initView() {
		title = (TextView) findViewById(R.id.tv_title);
		listView = (ListView) findViewById(R.id.select_list);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		Intent intent = new Intent(this, StartGameActivity.class);
		if (selectType.equals(SELECT_SUB_I)) {
			SubCoursesObject subCoursesObject = playGameObject
					.getCoursesObject().getSubCourses().get(position);
			if (subCoursesObject.getFairwayCount() >= RountContexts.FAIRWAY_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
				playGameObject.getSubCourseIdArray()[1] = 0;
				playGameObject.getSubCourseNameArray()[1] = null;
			} else {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
			}
			RountContexts.asyncParAndAerialPhotoBeans(playGameObject);
		} else if (selectType.equals(SELECT_SUB_II)) {
			SubCoursesObject subCoursesObject = playGameObject
					.getCoursesObject().getSubCourses().get(position);
			if (subCoursesObject.getFairwayCount() >= RountContexts.FAIRWAY_COUNT) {
				playGameObject.getSubCourseIdArray()[0] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[0] = subCoursesObject
						.getSubCoursesName();
				playGameObject.getSubCourseIdArray()[1] = 0;
				playGameObject.getSubCourseNameArray()[1] = null;
			} else {
				playGameObject.getSubCourseIdArray()[1] = subCoursesObject
						.getId();
				playGameObject.getSubCourseNameArray()[1] = subCoursesObject
						.getSubCoursesName();
				;
			}
			RountContexts.asyncParAndAerialPhotoBeans(playGameObject);
		} else if (selectType.equals(SELECT_HOLE)) {
			playGameObject.setStartHole(position);
			RountContexts.curHoleNumber = position;
		} else {
			playGameObject.setStartTee(list.get(position));
		}
		intent.putExtra(KeyValuesConstant.INTENT_PLAYGAMEOBJECT, playGameObject);
		setResult(RESULT_OK, intent);
		finish();
	}

	@Override
	public void onClick(View v) {
		if (R.id.btn_back == v.getId()) {
			finish();
		}
	}

}
