package cn.golfdigestchina.golfmaster.mastercourse.activities;

import android.support.v4.app.Fragment;
import android.util.Log;

import com.baidu.mobstat.StatService;

public abstract class StatFragment extends Fragment {
	public abstract String getPageName();

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		Log.e("stat", "startf" + getPageName());
		StatService.onPageStart(getActivity(), getPageName());
		super.onResume();

	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		Log.e("stat", "stopf" + getPageName());
		StatService.onPageEnd(getActivity(), getPageName());
		super.onPause();

	}
}
