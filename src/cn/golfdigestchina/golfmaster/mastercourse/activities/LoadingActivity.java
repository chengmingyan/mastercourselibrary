package cn.golfdigestchina.golfmaster.mastercourse.activities;

import java.io.File;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.KeyEvent;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.common.Contexts;
import cn.golfdigestchina.golfmaster.mastercourse.utils.FileUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.GpsSwitchUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.InternetUtil;

/**
 * 启动画面 主要功能：检查并打开GPS、检查网络、创建SD卡路径、拷贝数据
 * 
 * @Project GolfMate
 * @package com.teewell.golfmate.activities
 * @title LoadingActivity.java
 * @Description TODO
 * @author chengmingyan
 * @date 2013-1-15 14:28:30
 * @Copyright Copyright(C) 2013-1-15
 * @version 1.0.0
 */
@SuppressLint("HandlerLeak")
public class LoadingActivity extends ParentActivity {
	/**
	 * 启动tabhost的标志
	 */
	private static final int START_TABHOST = 10;
	private boolean isContinue = true;
	private Builder builder;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		setContentView(R.layout.loading);
		initSystem();
		super.onCreate(savedInstanceState);
	}

	@Override
	public String getPageName() {
		// TODO Auto-generated method stub
		return getString(R.string.baidu_page_course_loding);
	}

	@Override
	public void onResume() {

		// there is no need to check internet,directly jump to tabhost
		// checkIntenet();
		// makesure gps open
		checkGps();
		super.onResume();
	}

	/**
	 * 检查网络
	 */
	@SuppressWarnings("unused")
	private void checkIntenet() {
		if (InternetUtil.checkConnect(this)) {
			handler.sendEmptyMessageDelayed(START_TABHOST, 100);
		} else {
			final AlertDialog.Builder openWifiDialog = new Builder(this);
			openWifiDialog.setTitle(R.string.apk_title);
			openWifiDialog.setMessage(getString(R.string.intenet_not_connect)
					.toString());
			openWifiDialog.setPositiveButton(getString(R.string.ok).toString(),
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							InternetUtil.setWirless(LoadingActivity.this);
						}
					});
			openWifiDialog.setNegativeButton(getString(R.string.exit)
					.toString(), new OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
					finish();
				}
			});
			openWifiDialog.create();
			openWifiDialog.show();
			openWifiDialog.setCancelable(false);
		}
	}

	/**
	 * 检查Gps
	 */
	private void checkGps() {
		if (GpsSwitchUtil.ensureOpenGPS(this)) {
			// handler.sendEmptyMessageDelayed(START_TABHOST, 2500);
			checkIntenet();
		} else {
			final AlertDialog.Builder openGpsDialog = new Builder(this);
			openGpsDialog.setTitle(R.string.apk_title);
			openGpsDialog.setMessage(getString(R.string.set_gps).toString());
			openGpsDialog.setPositiveButton(getString(R.string.ok).toString(),
					new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							GpsSwitchUtil.startGPS(LoadingActivity.this);
						}
					});
			openGpsDialog.setNegativeButton(
					getString(R.string.exit).toString(), new OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					});
			openGpsDialog.create();
			openGpsDialog.show();
			openGpsDialog.setCancelable(false);
		}
	}

	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			super.handleMessage(msg);
			if (!isContinue) {
				return;
			}
			switch (msg.what) {
			case START_TABHOST: {

				Intent intent = new Intent(LoadingActivity.this,
						AroundActivity.class);
				startActivity(intent);
				finish();
			}
				break;
			}
		}
	};

	public float getVersion() {
		// 获取packagemanager的实例
		PackageManager packageManager = getPackageManager();
		// getPackageName()是你当前类的包名，0代表是获取版本信息
		PackageInfo packInfo;
		try {
			packInfo = packageManager.getPackageInfo(getPackageName(), 0);
			return packInfo.versionCode;
		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return -1;
	}

	/**
	 * 卡贝项目数据，创建文件路径
	 */
	private void initSystem() {
		// if (Environment.getExternalStorageState().equals(  
		// Environment.MEDIA_MOUNTED)) {// 表示sdcard存在，并且可以进
		// 行读写 
		// check sdcard,if not exist app exit
		if (!FileUtil.isExternalStorageState()) {
			AlertDialog.Builder builder = new Builder(this);
			builder.setTitle(R.string.app_name);
			builder.setMessage(R.string.not_sdcard_app_will_finish);
			builder.setPositiveButton(R.string.exit,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							finish();
						}
					});
			builder.create();
			builder.setCancelable(false);

		} else {
			// 取得sdcard文件路径
			File pathFile = android.os.Environment
					.getExternalStorageDirectory();
			android.os.StatFs statfs = new android.os.StatFs(pathFile.getPath());
			// 获取SDCard上每个block的SIZE
			long nBlocSize = statfs.getBlockSize();
			// 获取可供程序使用的Block的数量
			long nAvailaBlock = statfs.getAvailableBlocks();
			// 获取剩下的所有Block的数量(包括预留的一般程序无法使用的块)
			@SuppressWarnings("unused")
			long nFreeBlock = statfs.getFreeBlocks();
			// 计算 SDCard 剩余大小MB
			long nSDFreeSize = nAvailaBlock * nBlocSize / 1024 / 1024;
			if (nSDFreeSize < 6) {
				AlertDialog.Builder builder = new Builder(this);
				builder.setTitle(R.string.app_name);
				builder.setMessage("SDCard容量不足,请清除一些容量再试用本软件!");
				builder.setPositiveButton(R.string.exit,
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								finish();
							}
						});
				builder.create();
				builder.setCancelable(false);
			}
		}

		File file = new File(Contexts.SD_PATH + Contexts.APPLICATION_PATH);
		if (!file.exists()) {
			file.mkdirs();
		}

		// Create the course logo path
		File logo = new File(Contexts.LOGO_PATH);
		if (!logo.exists()) {
			logo.mkdirs();
		}

		// makesure gps open
		GpsSwitchUtil.ensureOpenGPS(this);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub

		// 判断用户是否按了返回键
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			isContinue = false;
			if (builder == null) {
				builder = new Builder(LoadingActivity.this);
				builder.setMessage(getString(R.string.exit_system_tip));
				builder.setTitle(getString(R.string.app_name));
				builder.setPositiveButton(getString(R.string.exit),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								builder = null;
								finish();
							}
						});
				builder.setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog,
									int which) {
								dialog.dismiss();
								builder = null;
								isContinue = true;
								handler.sendEmptyMessage(START_TABHOST);
							}
						});
				builder.create();
			}
			builder.show();
			builder.setCancelable(false);
		}
		return super.onKeyDown(keyCode, event);
	}
}
