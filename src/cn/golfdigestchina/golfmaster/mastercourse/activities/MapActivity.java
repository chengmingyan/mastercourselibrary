package cn.golfdigestchina.golfmaster.mastercourse.activities;

import java.util.List;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.AbsoluteLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.AerialPhotoBean;
import cn.golfdigestchina.golfmaster.mastercourse.beans.FairwayObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.PlayGameObject;
import cn.golfdigestchina.golfmaster.mastercourse.beans.ValidAerialPhotoBean;
import cn.golfdigestchina.golfmaster.mastercourse.common.Contexts;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.costants.KeyValuesConstant;
import cn.golfdigestchina.golfmaster.mastercourse.models.MapModel;
import cn.golfdigestchina.golfmaster.mastercourse.utils.BitmapUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.CoordinateUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.LocationUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.LocationUtil.RefreshLocationListener;
import cn.golfdigestchina.golfmaster.mastercourse.views.AssistView;

import com.baidu.location.BDLocation;
import com.polites.androidedited.GestureImageView;

/**
 * 航拍图界面 关键点：计算距离其实主要是两点的经纬度计算
 * 1,人的位置有两个地方决定：a,为获取到Gps时手指拖动，此时可以保存XY及经纬度；b,有Gps时刷新此时可以保存XY及经纬度 2，
 * 
 * @author chengmingyan
 */
public class MapActivity extends ParentActivity implements OnClickListener {
	private static final String TAG = MapActivity.class.getName();
	// 布局相关
	private GestureImageView aerialPhotoView;
	private AssistView assistView;
	private ProgressBar progressBar;
	private ImageView personView;
	private Bitmap flagBmp, flagBmp1, flagBmp2;
	// 数据相关
	private MapModel model;
	private CoordinateUtil util;
	private AerialPhotoBean aerialPhotoBean;
	private List<FairwayObject> holesList;
	private List<AerialPhotoBean> aerialPhotoBeans;
	private int lastHoleIndex;// 上一次球洞号
	// 常量相关
	private PlayGameObject playGameObject;
	private LocationUtil locationUtil;

	public static final float INITIAL_ROTATION = -90;
	private static final int WHAT_CHANGE_MAP = 0x0;
	public static final int WHAT_CHANGE_MENUBAR = 0x2;
	public static final int WHAT_CHANGE_COURSEBITMAP = 0x4;
	public static final int WHAT_DISTANCETRACKING = 0x5;
	public static final int WHAT_LOCATION_REFRESH = 0x10;
	public static final String ACTION_BROADCASTRECEIVER = "com.teewell.golfmate.activities.MapActivity.MapBroadcastReceiver";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		// 自初始化一次的
		playGameObject = (PlayGameObject) getIntent().getSerializableExtra(
				KeyValuesConstant.INTENT_PLAYGAMEOBJECT);
		model = MapModel.getInstance(this);
		lastHoleIndex = -1;
		locationUtil = LocationUtil.getInstance(this);
		initView();
		initData();
	}

	@Override
	public String getPageName() {
		// TODO Auto-generated method stub
		return getString(R.string.baidu_page_course_map);
	}

	private void initData() {
		holesList = RountContexts.getFairwayObjectArray(playGameObject);
		aerialPhotoBeans = playGameObject.getAerialPhotoBeanArray();
	}

	private void initView() {
		progressBar = (ProgressBar) findViewById(R.id.progressBar);
		aerialPhotoView = (GestureImageView) findViewById(R.id.photoView);
		assistView = (AssistView) findViewById(R.id.assistView);

		personView = (ImageView) findViewById(R.id.personView);
		personView.setBackgroundResource(R.drawable.person);

		/** 通过ImageView对象拿到背景显示的AnimationDrawable **/

		((TextView) findViewById(R.id.tv_title)).setText(playGameObject
				.getCoursesObject().getShortName());
		// 指示果岭的图片
		flagBmp = BitmapFactory.decodeResource(getResources(), R.drawable.flag);
		flagBmp1 = BitmapFactory.decodeResource(getResources(),
				R.drawable.flag1);
		flagBmp2 = BitmapFactory.decodeResource(getResources(),
				R.drawable.flag2);

		// flagBmp = BitmapUtil.zoomBitmap(flagBmp, 0.5f);
		// flagBmp1 = BitmapUtil.zoomBitmap(flagBmp1, 0.25f);
		// flagBmp2 = BitmapUtil.zoomBitmap(flagBmp2, 0.25f);
	}

	private void changeAerialPhotoThread() {
		aerialPhotoBean = aerialPhotoBeans.get(RountContexts.curHoleNumber);
		progressBar.setVisibility(View.VISIBLE);
		new Thread() {
			@Override
			public void run() {
				Bitmap bitmapOrg = BitmapUtil
						.readBitMapByNative(Contexts.AERIALPHOTO_PATH
								+ playGameObject.getCoursesObject()
										.getCouseID() + "/"
								+ aerialPhotoBean.getPhotoName() + "."
								+ aerialPhotoBean.getPhotoMode());
				Log.d(TAG, "bitmap.width" + bitmapOrg.getWidth()
						+ "-bitmap.height" + bitmapOrg.getHeight());
				int validArea[] = BitmapUtil.getValidArea(bitmapOrg);
				Message message = new Message();
				message.what = WHAT_CHANGE_MAP;
				ValidAerialPhotoBean bean = new ValidAerialPhotoBean();
				bean.setBitmap(bitmapOrg);
				bean.setValidArea(validArea);
				message.obj = bean;
				handler.sendMessage(message);
				super.run();
			}
		}.start();
	}

	@SuppressLint("HandlerLeak")
	Handler handler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			switch (msg.what) {
			case WHAT_CHANGE_MAP:
				progressBar.setVisibility(View.GONE);
				initMap((ValidAerialPhotoBean) msg.obj);
				break;
			case WHAT_LOCATION_REFRESH:
				assistView.invalidate();
				model.refreshPersonPoint(MapActivity.this);
				refreshPersonView();
				break;
			case WHAT_CHANGE_COURSEBITMAP:
				float ratio = aerialPhotoView.getScale();
				float bmpHeight = model.getCourseBitmap().getHeight() * ratio;
				float bmpWidth = model.getCourseBitmap().getWidth() * ratio;
				Log.e(TAG, "bitmapHeight=" + bmpHeight + ",bitmapWidth="
						+ bmpWidth);
				util.resetAttribute(ratio, aerialPhotoView.getImageX()
						- bmpWidth / 2, aerialPhotoView.getImageY() - bmpHeight
						/ 2, bmpWidth, bmpHeight);
				model.getGreenPointArray().clear();
				if (aerialPhotoBean.getHoleId()[1] > 0) {
					for (FairwayObject fairwayObject : holesList) {
						if (aerialPhotoBean.getAerialPhotoID().intValue() == fairwayObject
								.getAerialPhotoBean().getAerialPhotoID()
								.intValue()) {
							model.addGreenPoint(Double
									.parseDouble(fairwayObject
											.getGreenLongitude()), Double
									.parseDouble(fairwayObject
											.getGreenLatitude()));
						}
					}
				} else {
					for (FairwayObject fairwayObject : holesList) {
						if (aerialPhotoBean.getAerialPhotoID().intValue() == fairwayObject
								.getAerialPhotoBean().getAerialPhotoID()
								.intValue()) {
							model.addGreenPoint(Double
									.parseDouble(fairwayObject
											.getGreenLongitude()), Double
									.parseDouble(fairwayObject
											.getGreenLatitude()));
							break;
						}
					}
				}
				model.refreshData(MapActivity.this);
				refreshMarke();

				aerialPhotoView.invalidate();
				assistView.setNavigationMapModel(model, handler);
				assistView.invalidate();
				refreshPersonView();
				break;
			default:
				break;
			}
		};
	};

	private void initMap(ValidAerialPhotoBean bean) {
		Bitmap bitmapOrg = bean.getBitmap();
		int validArea[] = bean.getValidArea();
		try {
			if (bitmapOrg == null || aerialPhotoBean == null) {
				return;
			}

			util = new CoordinateUtil(validArea,
					aerialPhotoBean.getRotationView(),
					Double.parseDouble(aerialPhotoBean.getLuLongitude()),
					Double.parseDouble(aerialPhotoBean.getLuLatitude()),
					Double.parseDouble(aerialPhotoBean.getRdLongitude()),
					Double.parseDouble(aerialPhotoBean.getRdLatitude()));
			model.setCoordinateUtil(util);
			// 以下是将果岭旗帜画在航拍图上，生成新的航拍图保存，并使用新的航拍图作为背景
			Bitmap bitmap = Bitmap.createBitmap(bitmapOrg.getWidth(),
					bitmapOrg.getHeight(), Config.ARGB_8888);
			Canvas canvas = new Canvas(bitmap);
			Paint defaultPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
			// defaultPaint.setStyle(Paint.Style.STROKE);
			canvas.drawBitmap(bitmapOrg, 0, 0, defaultPaint);
			float bmpHeight = bitmapOrg.getHeight();
			float bmpWidth = bitmapOrg.getWidth();
			double onePxToLon = Math.abs((util.getLonRBottom() - util
					.getLonLTop()) / bmpWidth);
			double onePxToLat = Math.abs((util.getLatRBottom() - util
					.getLatLTop()) / bmpHeight);
			double midpointX = bmpWidth / 2;
			double midpointY = -bmpHeight / 2;
			float rotateAngle = util.getRotateAngle();
			int greenCount = 0;
			boolean isDoubleGreen = false;
			if (aerialPhotoBean.getHoleId()[1] > 0) {
				isDoubleGreen = true;
			}
			// model.getGreenPointArray().clear();
			for (FairwayObject fairwayObject : holesList) {
				if (aerialPhotoBean.getAerialPhotoID().intValue() == fairwayObject
						.getAerialPhotoBean().getAerialPhotoID().intValue()) {
					double greenLon = Double.parseDouble(fairwayObject
							.getGreenLongitude());
					double greenLat = Double.parseDouble(fairwayObject
							.getGreenLatitude());

					// model.addGreenPoint(greenLon, greenLat);
					double x1 = (greenLon - util.getLonLTop()) / onePxToLon;
					double y1 = (util.getLatLTop() - greenLat) / onePxToLat;
					y1 = -y1;
					float bitmapX = (float) (midpointX + (x1 - midpointX)
							* Math.cos(-rotateAngle * 2 * Math.PI / 360) - (y1 - midpointY)
							* Math.sin(-rotateAngle * 2 * Math.PI / 360));
					float bitmapY = (float) (midpointY + (x1 - midpointX)
							* Math.sin(-rotateAngle * 2 * Math.PI / 360) + (y1 - midpointY)
							* Math.cos(-rotateAngle * 2 * Math.PI / 360));
					bitmapY = -bitmapY;
					if (isDoubleGreen) {
						if (greenCount > 0) {
							canvas.drawBitmap(flagBmp2, bitmapX, bitmapY
									- flagBmp2.getHeight(), defaultPaint);
						} else {
							canvas.drawBitmap(flagBmp1, bitmapX, bitmapY
									- flagBmp1.getHeight(), defaultPaint);
							greenCount++;
						}
					} else {
						canvas.drawBitmap(flagBmp, bitmapX,
								bitmapY - flagBmp.getHeight(), defaultPaint);
						break;
					}

				}
			}
			if (!bitmapOrg.isRecycled()) {
				bitmapOrg.recycle();
			}
			if (model.getCourseBitmap() != null
					&& !model.getCourseBitmap().isRecycled()) {
				model.getCourseBitmap().recycle();
			}
			model.setCourseBitmap(bitmap);

			aerialPhotoView.setRecycle(true);
			aerialPhotoView.setImageBitmap(bitmap, model, assistView, handler);

		} catch (Exception e) {
			Log.e(TAG, "", e);
		}
	}

	private void refreshMarke() {
		lastHoleIndex = RountContexts.curHoleNumber;
	}

	@Override
	public void onPause() {
		super.onPause();
		unregisterReceiver(MapBroadcastReceiver);
		locationUtil.clearListener();
	}

	@Override
	public void onResume() {
		super.onResume();
		registerBoradcastReceiver();
		locationUtil.setListener(new RefreshLocationListener() {
			@Override
			public void refreshLocation(BDLocation location) {
				assistView.invalidate();
				model.refreshPersonPoint(MapActivity.this);
				refreshPersonView();
			}
		});
		changeMapByHoleID();
	}

	private void changeMapByHoleID() {
		((TextView) findViewById(R.id.tv_cn))
				.setText((RountContexts.curHoleNumber + 1) + "/"
						+ (aerialPhotoBeans.size()));
		((TextView) findViewById(R.id.tv_hole)).setText(holesList.get(
				RountContexts.curHoleNumber).getHoldNo()
				+ "洞(Par"
				+ holesList.get(RountContexts.curHoleNumber).getPoleNumber()
				+ ")");
		// 同一洞且子球场不改变
		if (lastHoleIndex == RountContexts.curHoleNumber
				&& !RountContexts.isChangeSubCourse) {
			return;
		}
		if (RountContexts.isChangeSubCourse) {
			initData();
			RountContexts.isChangeSubCourse = false;
		}

		changeAerialPhotoThread();

	}

	BroadcastReceiver MapBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			if (action.equals(ACTION_BROADCASTRECEIVER)) {
				// Toast.makeText(ObstadeActivity.this,
				// "处理action名字相对应的广播"+RountContexts.curHoleNumber, 200);
				Log.e(TAG, "map广播接收");
				changeMapByHoleID();
			}
		}
	};

	private void registerBoradcastReceiver() {
		IntentFilter myIntentFilter = new IntentFilter();
		myIntentFilter.addAction(ACTION_BROADCASTRECEIVER);
		// 注册广播
		registerReceiver(MapBroadcastReceiver, myIntentFilter);
	}

	@Override
	public void onClick(View v) {
		if (R.id.imb_next == v.getId()) {
			RountContexts.curHoleNumber += 1;
			if (aerialPhotoBeans.size() > RountContexts.curHoleNumber) {
				changeMapByHoleID();
			} else {
				RountContexts.curHoleNumber = aerialPhotoBeans.size() - 1;
			}
		} else if (R.id.imb_previous == v.getId()) {
			RountContexts.curHoleNumber -= 1;
			if (0 <= RountContexts.curHoleNumber) {
				changeMapByHoleID();
			} else {
				RountContexts.curHoleNumber = 0;
			}
		} else if (R.id.btn_left == v.getId()) {
			onBackPressed();
		}
	}

	public void refreshPersonView() {

		// 画小人
		int x = (int) (model.getPersonPoint().x - (float) personView
				.getMeasuredWidth() / 2);
		int y = (int) (model.getPersonPoint().y - (float) personView
				.getMeasuredHeight() / 2);
		if (x <= 0 || y <= 0) {
			personView.setVisibility(View.GONE);
		} else {
			personView.setVisibility(View.VISIBLE);
			personView
					.setLayoutParams(new AbsoluteLayout.LayoutParams(
							LayoutParams.WRAP_CONTENT,
							LayoutParams.WRAP_CONTENT, x, y));
		}

	}

}
