package cn.golfdigestchina.golfmaster.mastercourse.activities;

import java.util.ArrayList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.MarginLayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.common.Contexts;
import cn.golfdigestchina.golfmaster.mastercourse.common.RountContexts;
import cn.golfdigestchina.golfmaster.mastercourse.utils.DistanceDataUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.DistanceUtil;
import cn.golfdigestchina.golfmaster.mastercourse.utils.SharedPreferencesUtils;
import cn.golfdigestchina.golfmaster.mastercourse.utils.StringUtil;
import cn.golfdigestchina.golfmaster.mastercourse.views.SeekListAdapter;

public class CourseSettingActivity extends ParentActivity implements
		OnClickListener {
	// private final String TAG = CourseSettingActivity.class.getName();
	private SharedPreferencesUtils utils;
	private Button btn_add, btn_remove;
	private ListView list_seek;
	private TextView tv_distance_near, tv_distance_far;
	private RelativeLayout layout_distance_near, layout_distance_far;
	public static final int MIN_PROGRESS = 120;
	public static final int MAX_PROGRESS = 360;
	public static final int DISTANCE = 10;
	private int[] distanceArc;
	private String show_distance = "";
	private boolean isdel = false;
	private ArrayList<Integer> sbr_date;
	private SeekListAdapter seekListAdapter;
	private ArrayList<Integer> templist;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_course_setting);
		utils = new SharedPreferencesUtils(this);
		distanceArc = DistanceDataUtil.getDistanceArc(this);
		show_distance = DistanceUtil.getDistanceUnit(this,
				RountContexts.shortDistanceIndex);
		sbr_date = new ArrayList<Integer>();
		for (int i = 0; i < distanceArc.length; i++) {
			sbr_date.add(distanceArc[i]);
		}
		initView();
	}

	@Override
	public String getPageName() {
		// TODO Auto-generated method stub
		return getString(R.string.baidu_page_course_setting);
	}

	private void initData() {
		init(true);
	}

	private void init(boolean flag) {
		distanceArc = DistanceDataUtil.getDistanceArc(this);
		show_distance = DistanceUtil.getDistanceUnit(this,
				RountContexts.shortDistanceIndex);
		String distance_type_far = DistanceUtil.getDistanceUnit(this,
				RountContexts.longDistanceIndex);
		if (seekListAdapter != null) {
			seekListAdapter.setUnit(show_distance);
		}
		tv_distance_far.setText(distance_type_far);
		tv_distance_near.setText(show_distance);

	}

	private void initView() {
		TextView tv_title = (TextView) findViewById(R.id.tv_title);
		tv_title.setVisibility(View.VISIBLE);
		tv_title.setText("辅助设置");
		seekListAdapter = new SeekListAdapter(show_distance, sbr_date,
				getApplicationContext());
		btn_add = (Button) findViewById(R.id.btn_add);
		btn_remove = (Button) findViewById(R.id.btn_remove);
		list_seek = (ListView) findViewById(R.id.list_seekbar);
		tv_distance_near = (TextView) findViewById(R.id.tv_distance_near);
		tv_distance_far = (TextView) findViewById(R.id.tv_distance_far);
		layout_distance_far = (RelativeLayout) findViewById(R.id.layout_distance_far);
		layout_distance_near = (RelativeLayout) findViewById(R.id.layout_distance_near);
		list_seek.setAdapter(seekListAdapter);
		list_seek.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				System.out.println(isdel);
				if (isdel) {
					isdel = false;
					templist = seekListAdapter.getList();
					templist.remove(position);
					seekListAdapter.setList(templist);
					seekListAdapter.setIsdel(isdel);
				}
			}
		});
		btn_add.setOnClickListener(this);
		btn_remove.setOnClickListener(this);
		layout_distance_far.setOnClickListener(this);
		layout_distance_near.setOnClickListener(this);

		int totalHeight = 0;
		for (int i = 0; i < seekListAdapter.getCount(); i++) {
			View listItem = seekListAdapter.getView(i, null, list_seek);
			listItem.measure(0, 0);
			totalHeight += listItem.getMeasuredHeight();
		}

		ViewGroup.LayoutParams params = list_seek.getLayoutParams();
		params.height = totalHeight
				+ (list_seek.getDividerHeight() * (seekListAdapter.getCount() - 1));
		((MarginLayoutParams) params).setMargins(10, 10, 10, 10);
		list_seek.setLayoutParams(params);
	}

	@Override
	public void onClick(View v) {
		if (R.id.btn_back == v.getId()) {
			finish();
		} else if (R.id.btn_add == v.getId()) {
			templist = seekListAdapter.getList();
			if (templist.size() >= 5) {
				showToast("辅助线不能超过5条！");
			} else {
				templist.add(MIN_PROGRESS);
				seekListAdapter.setList(templist);
			}
		} else if (R.id.btn_remove == v.getId()) {
			if (isdel) {
				isdel = false;
				seekListAdapter.setIsdel(isdel);
				return;
			}
			templist = seekListAdapter.getList();
			if (templist.size() <= 0) {
				showToast("没有可删除的辅助线！");
			} else {
				isdel = true;
				seekListAdapter.setIsdel(isdel);
			}
		} else if (R.id.layout_distance_near == v.getId()) {
			Intent intent = new Intent(CourseSettingActivity.this,
					ShortDistanceSettingActivity.class);
			startActivity(intent);
		} else if (R.id.layout_distance_far == v.getId()) {
			Intent intent = new Intent(CourseSettingActivity.this,
					LongDistanceSettingActivity.class);
			startActivity(intent);
		}
	}

	@Override
	public void onResume() {
		initData();
		super.onResume();
	}

	@Override
	public void onPause() {
		templist = seekListAdapter.getList();
		if (templist.size() == 0) {
			utils.commitString(RountContexts.DISTANCEARC, "");
		} else {
			String[] strings = new String[templist.size()];
			for (int i = 0; i < templist.size(); i++) {
				strings[i] = String.valueOf(templist.get(i));
			}
			utils.commitString(RountContexts.DISTANCEARC,
					StringUtil.addString(strings, Contexts.STRING_SEPARATOR));
		}

		super.onPause();
	}

	@Override
	public void onBackPressed() {
		if (isdel) {
			isdel = false;
			seekListAdapter.setIsdel(isdel);
		} else
			super.onBackPressed();
	}
}
