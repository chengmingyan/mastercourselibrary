package cn.golfdigestchina.golfmaster.mastercourse.adapter;

import java.util.List;

import cn.golfdigestchina.golfmaster.mastercourse.R;
import cn.golfdigestchina.golfmaster.mastercourse.beans.ProvinceBean;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class ProvinceAdapter extends BaseAdapter {

	private List<String> mProvinces = null;
	private LayoutInflater mInflater = null;
	
	public ProvinceAdapter ( Context context, List<String> provinces ){
		this.mProvinces = provinces ;
		this.mInflater = LayoutInflater.from(context);
	}
	
	@Override
	public int getCount() {
		if( mProvinces==null ) {
			return 0;
		}
		return mProvinces.size();
	}

	@Override
	public Object getItem(int position) {
		if( mProvinces==null ) {
			return null;
		}
		return mProvinces.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder ;
		if( convertView == null ) {
			holder = new ViewHolder();
			convertView = mInflater.inflate(R.layout.layout_province_item, null);
			holder.tvProvince = (TextView) convertView.findViewById(R.id.tv_province);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		String province = (String) getItem(position);
		holder.tvProvince.setText(province);
		return convertView;
	}
	
	static class ViewHolder {
		private TextView tvProvince = null;

		public TextView getTvProvince() {
			return tvProvince;
		}
		public void setTvProvince(TextView tvProvince) {
			this.tvProvince = tvProvince;
		}
	}
	
}
